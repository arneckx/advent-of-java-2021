# Advent of code

[https://adventofcode.com/](https://adventofcode.com/)

Notes:

- Code has been written in Java.
- Code has **not** been written with a clean code mentality.
- Code may **not** be the most efficient way.
- Run tests to find solutions.

