package arneckx.year2023.day1;

import arneckx.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day1 extends Task<Integer, String> {

    public Day1(String inputFile) {
        super(inputFile);
    }

    List<CalibrationDigit> digits = List.of(
            new CalibrationDigit("1", 1),
            new CalibrationDigit("2", 2),
            new CalibrationDigit("3", 3),
            new CalibrationDigit("4", 4),
            new CalibrationDigit("5", 5),
            new CalibrationDigit("6", 6),
            new CalibrationDigit("7", 7),
            new CalibrationDigit("8", 8),
            new CalibrationDigit("9", 9),
            new CalibrationDigit("one", 1),
            new CalibrationDigit("two", 2),
            new CalibrationDigit("three", 3),
            new CalibrationDigit("four", 4),
            new CalibrationDigit("five", 5),
            new CalibrationDigit("six", 6),
            new CalibrationDigit("seven", 7),
            new CalibrationDigit("eight", 8),
            new CalibrationDigit("nine", 9)
    );

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        return createCalibrations()
                .stream()
                .mapToInt(a -> Integer.parseInt(a.getFirst().toString() + a.getLast().toString()))
                .sum();
    }

    @Override
    public Integer task2() {
        return createCalibrations2();
    }

    record CalibrationDigit(String toMatch, int amount) {

        public int indexOf(String line) {
            return line.indexOf(toMatch);
        }

        public int lastIndexOf(String line) {
            return line.lastIndexOf(toMatch);
        }
    }

    private int createCalibrations2() {
        List<Integer> numbers = new ArrayList<>();
        for (String line : input()) {
            int firstIndex = Integer.MAX_VALUE;
            int firstAmount = -1;
            int lastIndex = -1;
            int lastAmount = -1;
            for (CalibrationDigit digit : digits) {
                int index = digit.indexOf(line);
                int lastIndexOfDigit = digit.lastIndexOf(line);
                if (index > -1) {
                    if (firstIndex > index) {
                        firstIndex = index;
                        firstAmount = digit.amount;
                    }
                }
                if (lastIndexOfDigit > -1) {
                    if (lastIndex < lastIndexOfDigit) {
                        lastIndex = lastIndexOfDigit;
                        lastAmount = digit.amount;
                    }
                }
            }

            int digit = Integer.parseInt(firstAmount + String.valueOf(lastAmount));
            numbers.add(digit);
        }
        return numbers.stream().mapToInt(a -> a).sum();
    }

    private List<List<Integer>> createCalibrations() {
        List<List<Integer>> numbers = new ArrayList<>();
        for (String line : input()) {

            numbers.add(line.chars()
                    .filter(Character::isDigit)
                    .map(Character::getNumericValue)
                    .boxed()
                    .toList());
        }
        return numbers;
    }
}
