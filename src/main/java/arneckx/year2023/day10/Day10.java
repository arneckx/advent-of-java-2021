package arneckx.year2023.day10;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day10 extends Task<Integer, String> {

    public Day10(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        Result pipes = findPipes();
        List<List<Coordinate>> result = findLoop(pipes);
        int maxRoute = result.stream().mapToInt(List::size).max().orElseThrow();
        return (maxRoute + 1) / 2;
    }

    @Override
    public Integer task2() {
        Result pipes = findPipes();
        List<List<Coordinate>> result = findLoop(pipes);
        List<Coordinate> loop = result.get(0);
        Map<Integer, List<Coordinate>> collect = loop.stream().collect(Collectors.groupingBy(coordinate -> coordinate.x));
        int counter = 0;
        for (int x = 0; x < input().size(); x++) {
            Optional<List<Coordinate>> coordinates = Optional.ofNullable(collect.get(x));
            if (coordinates.isPresent()) {
                List<Coordinate> list = coordinates.get().stream().sorted(Comparator.comparing(Coordinate::y)).collect(Collectors.toList());

                int index = 0;
                while (index + 1 <= list.size() - 1) {
                    Coordinate coordinate1 = list.get(index);
                        Coordinate coordinate2 = list.get(index + 1);
                        for (int y = coordinate1.y; y <= coordinate2.y; y++) {
                            if (!pipes.pipesAtCoordinate.containsKey(new Coordinate(x, y))) {
                                // TODO: [main]
                                counter++;
                            }
                        }
                    index+=2;
                }
            }
        }

        return counter;
    }

    private List<List<Coordinate>> findLoop(Result pipes) {
        List<List<Coordinate>> result = new ArrayList<>();
        for (Coordinate coordinate : pipes.startPipe().connected()) {
            List<Coordinate> visited = new ArrayList<>();
            visited.add(pipes.startPipe().position());
            Optional<Pipe> nextPipe = Optional.ofNullable(pipes.pipesAtCoordinate().get(coordinate));
            if (nextPipe.isPresent() && nextPipe.get().connected().contains(pipes.startPipe().position())) {
                findLoop(nextPipe.get(), pipes.pipesAtCoordinate(), visited);
                result.add(visited);
            }
        }
        return result;
    }

    private Result findPipes() {
        Pipe startPipe = null;
        Map<Coordinate, Pipe> pipesAtCoordinate = new HashMap<>();

        for (int x = 0; x < input().size(); x++) {
            char[] line = input().get(x).toCharArray();
            for (int y = 0; y < line.length; y++) {
                Optional<Pipe> pipe = parseFrom(line[y], x, y);
                if (pipe.isPresent()) {
                    Pipe presentPipe = pipe.get();
                    pipesAtCoordinate.put(presentPipe.position(), presentPipe);
                    if (presentPipe instanceof Pipe.StartingPipe) {
                        startPipe = presentPipe;
                    }
                }
            }
        }
        if (startPipe == null) {
            throw new RuntimeException("No starting pipe");
        }
        return new Result(startPipe, pipesAtCoordinate);
    }

    private record Result(Pipe startPipe, Map<Coordinate, Pipe> pipesAtCoordinate) {
    }

    private void findLoop(Pipe currentPipe, Map<Coordinate, Pipe> pipesAtCoordinate, List<Coordinate> visited) {
        while (currentPipe != null) {
            visited.add(currentPipe.position());
            Optional<Pipe> nextPipe = getNext(currentPipe, pipesAtCoordinate, visited);
            if (nextPipe.isEmpty()) {
                return;
            }
            currentPipe = nextPipe.get();
        }
    }

    private Optional<Pipe> getNext(Pipe currentPipe, Map<Coordinate, Pipe> pipesAtCoordinate, List<Coordinate> visited) {
        return currentPipe.connected().stream()
                .map(pipesAtCoordinate::get)
                .filter(a -> Optional.ofNullable(a).isPresent())
                .filter(a -> !visited.contains(a.position()))
                .findFirst();
    }

    record Coordinate(int x, int y) {
    }

    public Optional<Pipe> parseFrom(Character character, int x, int y) {
        return switch (character) {
            case '|' -> Optional.of(new Pipe.VerticalPipe(new Coordinate(x, y), List.of(new Coordinate(x - 1, y), new Coordinate(x + 1, y))));
            case '-' -> Optional.of(new Pipe.HorizontalPipe(new Coordinate(x, y), List.of(new Coordinate(x, y - 1), new Coordinate(x, y + 1))));
            case 'L' -> Optional.of(new Pipe.BendFromAboveToRight(new Coordinate(x, y), List.of(new Coordinate(x - 1, y), new Coordinate(x, y + 1))));
            case 'J' -> Optional.of(new Pipe.BendFromAboveToLeft(new Coordinate(x, y), List.of(new Coordinate(x - 1, y), new Coordinate(x, y - 1))));
            case '7' -> Optional.of(new Pipe.BendFromBelowToLeft(new Coordinate(x, y), List.of(new Coordinate(x + 1, y), new Coordinate(x, y - 1))));
            case 'F' -> Optional.of(new Pipe.BendFromBelowToRight(new Coordinate(x, y), List.of(new Coordinate(x + 1, y), new Coordinate(x, y + 1))));
            case 'S' -> Optional.of(new Pipe.StartingPipe(new Coordinate(x, y),
                    List.of(new Coordinate(x, y - 1), new Coordinate(x, y + 1), new Coordinate(x - 1, y), new Coordinate(x + 1, y))
            ));
            case '.' -> Optional.empty();
            default -> throw new RuntimeException("unknown character");
        };
    }

    public sealed interface Pipe {

        Coordinate position();

        List<Coordinate> connected();

        record VerticalPipe(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record HorizontalPipe(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record BendFromAboveToRight(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record BendFromAboveToLeft(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record BendFromBelowToLeft(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record BendFromBelowToRight(Coordinate position, List<Coordinate> connected) implements Pipe {
        }

        record StartingPipe(Coordinate position, List<Coordinate> connected) implements Pipe {
        }
    }
}
