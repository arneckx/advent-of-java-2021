package arneckx.year2023.day5;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day5 extends Task<Long, String> {

    private Map<Long, MapEntry> seedToSoil;
    private Map<Long, MapEntry> soilToFertilizer;
    private Map<Long, MapEntry> fertilizerToWater;
    private Map<Long, MapEntry> waterToLight;
    private Map<Long, MapEntry> lightToTemperature;
    private Map<Long, MapEntry> temperatureToHumidity;
    private Map<Long, MapEntry> humidityToLocation;

    public Day5(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long task1() {
        List<String> input = input();
        parseInput();
        List<Long> seeds = Arrays.stream(input.get(0).split("seeds: ")[1].split(" ")).map(Long::parseLong).toList();
        return firstApproach(seeds);
    }

    @Override
    public Long task2() {
        List<String> input = input();
        parseInput();
        List<Long> seeds = Arrays.stream(input.get(0).split("seeds: ")[1].split(" ")).map(Long::parseLong).toList();
        List<StartEnd> allSeeds = new ArrayList<>();
        for (int i = 0; i < seeds.size(); i += 2) {
            Long start = seeds.get(i);
            Long length = seeds.get(i + 1);
            allSeeds.add(new StartEnd(start, start + length));
        }
        return secondApproach(allSeeds);
    }

    private long firstApproach(List<Long> seeds) {
        long result = seeds.stream()
                .mapToLong(seed -> {
                    long soil = findFirstMathingFor(seed, seedToSoil);
                    long fertilizer = findFirstMathingFor(soil, soilToFertilizer);
                    long water = findFirstMathingFor(fertilizer, fertilizerToWater);
                    long light = findFirstMathingFor(water, waterToLight);
                    long temperature = findFirstMathingFor(light, lightToTemperature);
                    long humidity = findFirstMathingFor(temperature, temperatureToHumidity);
                    long location = findFirstMathingFor(humidity, humidityToLocation);
                    return location;
                })
                .min()
                .orElseThrow();
        return result;
    }

    private long secondApproach(List<StartEnd> seeds) {
        Optional<Long> result = Optional.empty();
        long counter = 0L;
        while (result.isEmpty()) {
            long humidity = findFirstMathingForDestination(counter, humidityToLocation);
            long temperature = findFirstMathingForDestination(humidity, temperatureToHumidity);
            long light = findFirstMathingForDestination(temperature, lightToTemperature);
            long water = findFirstMathingForDestination(light, waterToLight);
            long fertilizer = findFirstMathingForDestination(water, fertilizerToWater);
            long soil = findFirstMathingForDestination(fertilizer, soilToFertilizer);
            long seed = findFirstMathingForDestination(soil, seedToSoil);
            Optional<StartEnd> maybeSeed = seeds.stream().filter(aSeed -> aSeed.start <= seed && seed <= aSeed.end).findFirst();
            if (maybeSeed.isPresent()) {
                result = Optional.of(counter);
            }
            counter++;
        }

        return result.get();
    }

    public long findFirstMathingFor(long source, Map<Long, MapEntry> map) {
        return map.values()
                .stream()
                .filter(entry -> entry.matches(source))
                .mapToLong(entry -> entry.destinationValueFor(source))
                .findFirst()
                .orElse(source);
    }

    public long findFirstMathingForDestination(long destination, Map<Long, MapEntry> map) {
        return map.values()
                .stream()
                .filter(entry -> entry.matchesDestination(destination))
                .mapToLong(entry -> entry.sourceValueFor(destination))
                .findFirst()
                .orElse(destination);
    }

    record MapEntry(StartEnd destination, StartEnd source, long range) {

        static MapEntry fromInput(String line) {
            String[] split = line.split(" ");
            long destinationStart = Long.parseLong(split[0]);
            long sourceStart = Long.parseLong(split[1]);
            long range = Long.parseLong(split[2]);
            return new MapEntry(new StartEnd(destinationStart, destinationStart + range), new StartEnd(sourceStart, sourceStart + range), range);
        }

        public boolean matches(Long lookupSource) {
            return source.start <= lookupSource && lookupSource <= source.end;
        }

        public Long destinationValueFor(Long lookupSource) {
            return destination.start + (lookupSource - source.start);
        }

        public boolean matchesDestination(long lookupDestination) {
            return destination.start <= lookupDestination && lookupDestination <= destination.end;
        }

        public long sourceValueFor(long lookupDestination) {
            return source.start + (lookupDestination - destination.start);
        }
    }

    record StartEnd(long start, long end) {
    }

    public void parseInput() {
        List<String> input = input();
        int soilToFertilizerIndex = input.indexOf("soil-to-fertilizer map:");
        seedToSoil = IntStream.range(3, soilToFertilizerIndex - 1).mapToObj(index -> MapEntry.fromInput(
                input.get(index))).collect(Collectors.toMap(me -> me.source().start(), me -> me));
        int fertilizerToWaterIndex = input.indexOf("fertilizer-to-water map:");
        soilToFertilizer = IntStream.range(soilToFertilizerIndex + 1, fertilizerToWaterIndex - 1).mapToObj(index -> MapEntry.fromInput(
                input.get(index))).collect(Collectors.toMap(me -> me.source().start(), me -> me));
        int waterToLightIndex = input.indexOf("water-to-light map:");
        fertilizerToWater = IntStream.range(fertilizerToWaterIndex + 1, waterToLightIndex - 1).mapToObj(index -> MapEntry.fromInput(input.get(index)))
                .collect(Collectors.toMap(me -> me.source().start(), me -> me));
        int lightToTemperatureIndex = input.indexOf("light-to-temperature map:");
        waterToLight = IntStream.range(waterToLightIndex + 1, lightToTemperatureIndex - 1).mapToObj(index -> MapEntry.fromInput(input.get(index)))
                .collect(Collectors.toMap(me -> me.source().start(), me -> me));
        int temperatureToHumidityIndex = input.indexOf("temperature-to-humidity map:");
        lightToTemperature = IntStream.range(lightToTemperatureIndex + 1, temperatureToHumidityIndex - 1).mapToObj(index -> MapEntry.fromInput(
                input.get(index))).collect(Collectors.toMap(me -> me.source().start(), me -> me));
        int humidityToLocationIndex = input.indexOf("humidity-to-location map:");
        temperatureToHumidity = IntStream.range(temperatureToHumidityIndex + 1, humidityToLocationIndex - 1).mapToObj(index -> MapEntry.fromInput(
                input.get(index))).collect(Collectors.toMap(me -> me.source().start(), me -> me));
        humidityToLocation = IntStream.range(humidityToLocationIndex + 1, input.size())
                .mapToObj(index -> MapEntry.fromInput(input.get(index)))
                .collect(Collectors.toMap(me -> me.source().start(), me -> me));
    }
}
