package arneckx.year2023.day8;

import arneckx.Task;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day8 extends Task<Long, String> {

    public Day8(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long task1() {
        int steps = 0;
        List<Instruction> instructions = input().get(0).chars().mapToObj(c -> Instruction.from((char) c)).toList();
        Map<String, Node> map = input().stream()
                .skip(2)
                .map(this::toNode)
                .collect(Collectors.toMap(Node::id, Function.identity()));

        Node currentNode = map.get("AAA");

        while (!currentNode.id().equals("ZZZ")) {
            for (Instruction instruction : instructions) {
                steps++;
                switch (instruction) {
                    case LEFT -> currentNode = map.get(currentNode.leftNodeId);
                    case RIGHT -> currentNode = map.get(currentNode.rightNodeId);
                }
            }
        }

        return (long) steps;
    }

    @Override
    public Long task2() {
        List<Instruction> instructions = input().get(0).chars().mapToObj(c -> Instruction.from((char) c)).toList();
        Map<String, Node> map = input().stream()
                .skip(2)
                .map(this::toNode)
                .collect(Collectors.toMap(Node::id, Function.identity()));

        return map.values()
                .stream()
                .filter(node1 -> node1.lastCharacter == 'A').toList().stream()
                .map(node -> nodeToZ(node, instructions, map))
                .mapToLong(startToEnd -> startToEnd.steps)
                .reduce((a, b) -> lowestCommonMultiplier(BigInteger.valueOf(a), BigInteger.valueOf(b)).longValue())
                .orElseThrow();
    }

    public BigInteger lowestCommonMultiplier(BigInteger number1, BigInteger number2) {
        BigInteger gcd = number1.gcd(number2);
        BigInteger absProduct = number1.multiply(number2).abs();
        return absProduct.divide(gcd);
    }

    private StartToEnd nodeToZ(Node node, List<Instruction> instructions, Map<String, Node> map) {
        int stepsForNode = 0;
        Node currentNode = node;
        while (!(currentNode.lastCharacter() == 'Z')) {
            for (Instruction instruction : instructions) {
                stepsForNode++;
                switch (instruction) {
                    case LEFT -> currentNode = map.get(currentNode.leftNodeId);
                    case RIGHT -> currentNode = map.get(currentNode.rightNodeId);
                }
            }
        }
        return new StartToEnd(
                node,
                currentNode,
                stepsForNode
        );
    }

    record StartToEnd(Node startNode, Node endNode, int steps) {
    }

    private Node toNode(String line) {
        String[] split = line.split(" = ");
        String[] leftRightSplit = split[1].replace("(", "").replace(")", "").split(", ");
        return new Node(
                split[0],
                split[0].chars().skip(2).mapToObj(c -> (char) c).findFirst().orElseThrow(),
                leftRightSplit[0],
                leftRightSplit[1]
        );
    }

    record Node(String id, Character lastCharacter, String leftNodeId, String rightNodeId) {
    }

    enum Instruction {
        LEFT('L'),
        RIGHT('R');

        private static final Map<Character, Instruction> instructionMap =
                Arrays.stream(Instruction.values()).collect(Collectors.toMap(Instruction::character, Function.identity()));

        private final Character character;

        Instruction(Character character) {
            this.character = character;
        }

        static Instruction from(Character character) {
            return instructionMap.get(character);
        }

        public Character character() {
            return character;
        }
    }
}
