package arneckx.year2023.day9;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Day9 extends Task<Long, String> {

    public Day9(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long task1() {
        return histories().stream().mapToLong(History::nextValue).sum();
    }

    @Override
    public Long task2() {
        return histories().stream().mapToLong(History::previousValue).sum();
    }

    private List<History> histories() {
        List<History> histories = new ArrayList<>();
        for (String line : input()) {
            histories.add(new History(Arrays.stream(line.split(" "))
                    .filter(a -> !a.isBlank())
                    .map(Long::parseLong)
                    .toList()));
        }
        return histories;
    }

    class History {

        private final Long nextValue;
        private final Long previousValue;

        History(List<Long> originalHistory) {
            List<List<Long>> allSequences = new ArrayList<>();
            allSequences.add(originalHistory);
            List<Long> currentSequence = originalHistory;
            long fistValue = originalHistory.getFirst();
            long lastValue = originalHistory.getLast();

            System.out.println(currentSequence);
            while (lastValue != 0) {
                List<Long> nextSequence = new ArrayList<>();
                for (int i = 0; i < currentSequence.size() - 1; i++) {
                    nextSequence.add((currentSequence.get(i + 1)) - (currentSequence.get(i)));
                }
                currentSequence = nextSequence;
                allSequences.add(currentSequence);
                fistValue = currentSequence.getFirst();
                lastValue = currentSequence.getLast();
                System.out.println(nextSequence);
            }

            System.out.println("\n");


            for (List<Long> allSequence : allSequences.reversed()) {
                fistValue = allSequence.getFirst() - fistValue;
                lastValue = allSequence.getLast() + lastValue;
            }

            System.out.println(fistValue);
            System.out.println(lastValue);
            System.out.println("\n");
            this.nextValue = lastValue;
            this.previousValue = fistValue;
        }

        long nextValue() {
            return nextValue;
        }

        long previousValue() {
            return previousValue;
        }
    }
}
