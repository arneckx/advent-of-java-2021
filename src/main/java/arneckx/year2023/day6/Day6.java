package arneckx.year2023.day6;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Day6 extends Task<Integer, String> {

    public Day6(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        return getRaces().stream().mapToInt(r -> r.possibleButtonPressesToWin().size()).reduce(1, (a, b) -> a * b);
    }

    @Override
    public Integer task2() {
        return getSingleRace().possibleButtonPressesToWin().size();
    }

    public Race getSingleRace() {
        List<String> input = input();
        long time = Long.parseLong(input.get(0).split(":")[1].trim().replace(" ", ""));
        long distance = Long.parseLong(input.get(1).split(":")[1].trim().replace(" ", ""));

        return new Race(0, time, distance);
    }

    public List<Race> getRaces() {
        List<String> input = input();
        List<Integer> times =
                Arrays.stream(input.get(0).split(":")[1].trim().split(" ")).map(String::trim).filter(a -> !a.isBlank()).map(Integer::parseInt).toList();
        List<Integer> distances =
                Arrays.stream(input.get(1).split(":")[1].trim().split(" ")).map(String::trim).filter(a -> !a.isBlank()).map(Integer::parseInt).toList();

        List<Race> races = new ArrayList<>();
        for (int index = 0; index < times.size(); index++) {
            races.add(new Race(0, times.get(index), distances.get(index)));
        }

        return races;
    }

    class Race {

        private final List<Long> possibleButtonPressesToWin = new ArrayList<>();

        public Race(long startSpeed, long time, long recordDistance) {
            for (long timePressingTheButton = 0; timePressingTheButton <= time; timePressingTheButton++) {
                long result = (time - timePressingTheButton) * (startSpeed + timePressingTheButton);
                if (result > recordDistance) {
                    possibleButtonPressesToWin.add(timePressingTheButton);
                }
            }
        }

        public List<Long> possibleButtonPressesToWin() {
            return possibleButtonPressesToWin;
        }
    }
}
