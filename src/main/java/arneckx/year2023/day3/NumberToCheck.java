package arneckx.year2023.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class NumberToCheck {

    private final int number;
    private final List<Day3.Coordinate> toCheck = new ArrayList<>();

    public NumberToCheck(String number, int rowIndex, int columnIndex) {
        for (int i = -1; i <= number.length(); i++) {
            toCheck.add(new Day3.Coordinate(rowIndex - 1, columnIndex + i));
            toCheck.add(new Day3.Coordinate(rowIndex + 1, columnIndex + i));
        }
        toCheck.add(new Day3.Coordinate(rowIndex, columnIndex - 1));
        toCheck.add(new Day3.Coordinate(rowIndex, columnIndex + number.length()));

        this.number = Integer.parseInt(number);
    }

    public int number() {
        return number;
    }

    public List<Day3.Coordinate> toCheck() {
        return toCheck;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NumberToCheck that = (NumberToCheck) o;
        return number == that.number && Objects.equals(toCheck, that.toCheck);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, toCheck);
    }

    @Override
    public String toString() {
        return "NumberToCheck{" +
                "number=" + number +
                ", toCheck=" + toCheck +
                '}';
    }
}
