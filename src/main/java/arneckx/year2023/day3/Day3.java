package arneckx.year2023.day3;

import arneckx.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day3 extends Task<Integer, String> {

    public Day3(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        int counter = 0;
        EngineSchema engineSchema = getEngineSchema();

        for (NumberToCheck numberToCheck : engineSchema.numbersToChecks()) {
            if (numberToCheck.toCheck().stream()
                    .anyMatch(toCheck -> engineSchema.characters().get(toCheck) != null)) {
                counter += numberToCheck.number();
            }
        }

        return counter;
    }

    @Override
    public Integer task2() {
        int counter = 0;
        GearSchema gearSchema = getGearSchema();

        for (GearToCheck gear : gearSchema.gearToCheck()) {
            Set<NumberToCheck> numberToChecks = gear.toCheck().stream()
                    .map(toCheck -> gearSchema.numbers().get(toCheck))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            if (numberToChecks.size() > 1) {
                int gearNumber = numberToChecks.stream().mapToInt(NumberToCheck::number).reduce(1, (a, b) -> a * b);
                counter += gearNumber;
            }
        }
        return counter;
    }

    private EngineSchema getEngineSchema() {
        List<NumberToCheck> numbersToChecks = new ArrayList<>();
        Map<Coordinate, Character> characters = new HashMap<>();
        for (int rowIndex = 0; rowIndex < input().size(); rowIndex++) {
            String line = input().get(rowIndex);

            Pattern integerPattern = Pattern.compile("0|[1-9]\\d*");
            Matcher numericMatcher = integerPattern.matcher(line);
            while (numericMatcher.find()) {
                numbersToChecks.add(new NumberToCheck(numericMatcher.group(), rowIndex, numericMatcher.start()));
            }

            Pattern characterPattern = Pattern.compile("[*+!\"`'#%&,:;<>=@{}~$/-]");
            Matcher characterMatcher = characterPattern.matcher(line);
            while (characterMatcher.find()) {
                String group = characterMatcher.group();
                Coordinate coordinate = new Coordinate(rowIndex, characterMatcher.start());
                characters.put(coordinate, new Character(group, coordinate));
            }
        }
        return new EngineSchema(
                numbersToChecks,
                characters
        );
    }

    private GearSchema getGearSchema() {
        List<GearToCheck> gearsToChecks = new ArrayList<>();
        Map<Coordinate, NumberToCheck> numbers = new HashMap<>();
        for (int rowIndex = 0; rowIndex < input().size(); rowIndex++) {
            String line = input().get(rowIndex);

            Pattern integerPattern = Pattern.compile("0|[1-9]\\d*");
            Matcher numericMatcher = integerPattern.matcher(line);
            while (numericMatcher.find()) {
                String group = numericMatcher.group();
                NumberToCheck numberToCheck = new NumberToCheck(numericMatcher.group(), rowIndex, numericMatcher.start());
                for (int i = 0; i < group.length(); i++) {
                    Coordinate coordinate = new Coordinate(rowIndex, numericMatcher.start() + i);
                    numbers.put(coordinate, numberToCheck);
                }
            }

            Pattern characterPattern = Pattern.compile("[*]");
            Matcher characterMatcher = characterPattern.matcher(line);
            while (characterMatcher.find()) {
                String group = characterMatcher.group();
                gearsToChecks.add(new GearToCheck(group, rowIndex, characterMatcher.start()));
            }
        }
        return new GearSchema(
                gearsToChecks,
                numbers
        );
    }

    record GearSchema(List<GearToCheck> gearToCheck, Map<Coordinate, NumberToCheck> numbers) {
    }

    record EngineSchema(List<NumberToCheck> numbersToChecks, Map<Coordinate, Character> characters) {
    }

    record Character(String character, Day3.Coordinate coordinate) {
    }

    record Coordinate(int x, int y) {
    }
}
