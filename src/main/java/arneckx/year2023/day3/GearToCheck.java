package arneckx.year2023.day3;

import java.util.ArrayList;
import java.util.List;

class GearToCheck {

    private final List<Day3.Coordinate> toCheck = new ArrayList<>();

    public GearToCheck(String character, int rowIndex, int columnIndex) {
        for (int i = -1; i <= character.length(); i++) {
            toCheck.add(new Day3.Coordinate(rowIndex - 1, columnIndex + i));
            toCheck.add(new Day3.Coordinate(rowIndex + 1, columnIndex + i));
        }
        toCheck.add(new Day3.Coordinate(rowIndex, columnIndex - 1));
        toCheck.add(new Day3.Coordinate(rowIndex, columnIndex + character.length()));
    }

    public List<Day3.Coordinate> toCheck() {
        return toCheck;
    }
}
