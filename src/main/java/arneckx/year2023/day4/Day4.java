package arneckx.year2023.day4;

import arneckx.Task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day4 extends Task<Integer, String> {

    public Day4(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        int totalScore = input().stream()
                .map(this::mapToCard)
                .mapToInt(Card::score)
                .sum();

        return totalScore;
    }

    @Override
    public Integer task2() {
        int totalScore = 0;
        List<Card> cards = input().stream()
                .map(this::mapToCard)
                .toList();
        Map<Integer, Integer> copiesOfEachCard = new HashMap<>();

        for (Card card : cards) {
            int instancesOfCard = copiesOfEachCard.computeIfAbsent(card.gameNumber(), (key) -> 0) + 1;
            totalScore += instancesOfCard;
            for (Integer gameNumberToCopy : card.gamesToCopy()) {
                copiesOfEachCard.put(gameNumberToCopy, copiesOfEachCard.computeIfAbsent(gameNumberToCopy, (key) -> 0) + instancesOfCard);
            }
        }

        return totalScore;
    }

    private Card mapToCard(String line) {
        String[] splitOnColon = line.split(":");
        int gameNumber = Integer.parseInt(splitOnColon[0].replace("Card ", "").trim());

        String[] splitOnPipe = splitOnColon[1].split("\\|");
        Set<Integer> winningNumbers = Arrays.stream(splitOnPipe[0].trim().split(" "))
                .filter(element -> !element.isBlank())
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
        Set<Integer> yourNumbers = Arrays.stream(splitOnPipe[1].trim().split(" "))
                .filter(element -> !element.isBlank())
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
        return new Card(gameNumber, winningNumbers, yourNumbers);
    }

    static class Card {

        private final int gameNumber;
        private final Set<Integer> winningNumbers;
        private final Set<Integer> yourNumber;
        private final int score;
        private final Set<Integer> gamesToCopy;

        public Card(int gameNumber, Set<Integer> winningNumbers, Set<Integer> yourNumbers) {
            Set<Integer> gamesToCopy = new HashSet<>();
            int gameToCopy = gameNumber;
            int score = 0;
            for (Integer yourNumber : yourNumbers) {
                if (winningNumbers.contains(yourNumber)) {
                    gamesToCopy.add(++gameToCopy);
                    if (score == 0) {
                        score = 1;
                    } else {
                        score *= 2;
                    }
                }
            }
            this.gameNumber = gameNumber;
            this.winningNumbers = winningNumbers;
            this.yourNumber = yourNumbers;
            this.score = score;
            this.gamesToCopy = gamesToCopy;
        }

        public int score() {
            return score;
        }

        public Set<Integer> gamesToCopy() {
            return gamesToCopy;
        }

        public int gameNumber() {
            return gameNumber;
        }
    }
}
