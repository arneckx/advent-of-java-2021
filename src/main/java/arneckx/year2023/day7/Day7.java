package arneckx.year2023.day7;

import arneckx.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day7 extends Task<Integer, String> {

    public Day7(String inputFile) {
        super(inputFile);
    }

    private boolean jIsWeakest = false;

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        jIsWeakest = false;
        int result = 0;
        List<Hand> hands = getHands(this::parseTask1);
        hands.sort(Comparator.comparing(Hand::strongness)
                .thenComparingInt(hand -> hand.cards().get(0).strongness(false))
                .thenComparingInt(hand -> hand.cards().get(1).strongness(false))
                .thenComparingInt(hand -> hand.cards().get(2).strongness(false))
                .thenComparingInt(hand -> hand.cards().get(3).strongness(false))
                .thenComparingInt(hand -> hand.cards().get(4).strongness(false))
        );
        for (int rank = 1; rank <= hands.size(); rank++) {
            Hand hand = hands.get(rank - 1);
            result += rank * hand.bidAmount();
        }
        return result;
    }

    @Override
    public Integer task2() {
        jIsWeakest = true;
        int result = 0;
        List<Hand> hands = getHands(this::parseTask2);
        hands.sort(Comparator.comparing(Hand::strongness)
                .thenComparingInt(hand -> hand.cards().get(0).strongness(true))
                .thenComparingInt(hand -> hand.cards().get(1).strongness(true))
                .thenComparingInt(hand -> hand.cards().get(2).strongness(true))
                .thenComparingInt(hand -> hand.cards().get(3).strongness(true))
                .thenComparingInt(hand -> hand.cards().get(4).strongness(true))
        );
        for (int rank = 1; rank <= hands.size(); rank++) {
            Hand hand = hands.get(rank - 1);
            result += rank * hand.bidAmount();
        }
        return result;
    }

    private List<Hand> getHands(Function<String, Hand> handSupplier) {
        return input().stream()
                .map(handSupplier)
                .collect(Collectors.toList());
    }

    private Result parse(String line) {
        String[] split = line.split(" ");
        String allCards = split[0];
        List<Card> cards = allCards.chars().mapToObj(character -> new Card((char) character)).toList();
        int bidAmount = Integer.parseInt(split[1]);

        Map<Character, Long> matchesMap = new HashMap<>();

        for (int i = 0; i < 5; i++) {
            char aChar = cards.get(i).character();
            matchesMap.put(aChar, allCards.chars().filter(ch -> ch == aChar).count());
        }
        Result result = new Result(cards, bidAmount, matchesMap);
        return result;
    }

    private Hand parseTask2(String line) {
        Result result = parse(line);
        long amountOfJokers = Optional.ofNullable(result.matchesMap.get('J')).orElse(0L);
        Collection<Long> matchingCards = result.matchesMap().entrySet().stream().filter(a -> a.getKey() != 'J').map(Map.Entry::getValue).toList();
        long max = matchingCards.stream().mapToLong(a -> a).max().orElse(0L);

        if (max + amountOfJokers == 5L) {
            return new Hand.FiveOfAKind(result.cards(), result.bidAmount());
        } else if (max + amountOfJokers == 4L) {
            return new Hand.FourOfAKind(result.cards(), result.bidAmount());
        } else if (
                (matchingCards.stream().filter(number -> number == 2).count() == 2 && amountOfJokers == 1) ||
                        (matchingCards.contains(3L) && matchingCards.contains(2L)) ||
                        (amountOfJokers == 3L && matchingCards.contains(2L)) ||
                        (amountOfJokers == 2L && matchingCards.contains(3L))
        ) {
            return new Hand.FullHouse(result.cards(), result.bidAmount());
        } else if (max + amountOfJokers == 3L) {
            return new Hand.ThreeOfAKind(result.cards(), result.bidAmount());
        } else if (
                (matchingCards.stream().filter(number -> number == 2).count() == 2) ||
                        matchingCards.contains(2L) && amountOfJokers > 1
        ) {
            return new Hand.TwoPair(result.cards(), result.bidAmount());
        } else if (max + amountOfJokers == 2L) {
            return new Hand.OnePair(result.cards(), result.bidAmount());
        } else {
            return new Hand.HighCard(result.cards(), result.bidAmount());
        }
    }

    private record Result(List<Card> cards, int bidAmount, Map<Character, Long> matchesMap) {
    }

    private Hand parseTask1(String line) {
        Result result = parse(line);
        Collection<Long> matchingCards = result.matchesMap().values();
        if (matchingCards.contains(5L)) {
            return new Hand.FiveOfAKind(result.cards(), result.bidAmount());
        } else if (matchingCards.contains(4L)) {
            return new Hand.FourOfAKind(result.cards(), result.bidAmount());
        } else if (matchingCards.contains(3L) && matchingCards.contains(2L)) {
            return new Hand.FullHouse(result.cards(), result.bidAmount());
        } else if (matchingCards.contains(3L)) {
            return new Hand.ThreeOfAKind(result.cards(), result.bidAmount());
        } else if (matchingCards.stream().filter(number -> number == 2).count() == 2) {
            return new Hand.TwoPair(result.cards(), result.bidAmount());
        } else if (matchingCards.contains(2L)) {
            return new Hand.OnePair(result.cards(), result.bidAmount());
        } else {
            return new Hand.HighCard(result.cards(), result.bidAmount());
        }
    }

    record Card(Character character) {

        public int strongness(boolean jokerIsWeakest) {
            return switch (String.valueOf(character)) {
                case "A" -> 14;
                case "K" -> 13;
                case "Q" -> 12;
                case "J" -> jokerIsWeakest ? 0 : 11;
                case "T" -> 10;
                case "9" -> 9;
                case "8" -> 8;
                case "7" -> 7;
                case "6" -> 6;
                case "5" -> 5;
                case "4" -> 4;
                case "3" -> 3;
                case "2" -> 2;
                case "1" -> 1;
                default -> throw new IllegalArgumentException();
            };
        }
    }

    public sealed interface Hand {

        int bidAmount();

        List<Card> cards();

        int strongness();

        record FiveOfAKind(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 7;
            }
        }

        record FourOfAKind(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 6;
            }
        }

        record FullHouse(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 5;
            }
        }

        record ThreeOfAKind(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 4;
            }
        }

        record TwoPair(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 3;
            }
        }

        record OnePair(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 2;
            }
        }

        record HighCard(List<Card> cards, int bidAmount) implements Hand {

            @Override
            public int strongness() {
                return 1;
            }
        }
    }
}
