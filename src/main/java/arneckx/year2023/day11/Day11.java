package arneckx.year2023.day11;

import arneckx.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day11 extends Task<Long, String> {

    public Day11(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long task1() {
        ParseResult parseResult = parse();
        return calculateDistance(parseResult.image, parseResult.pairs, 2);
    }

    @Override
    public Long task2() {
        ParseResult parseResult = parse();
        return calculateDistance(parseResult.image, parseResult.pairs, 1000000);
    }

    private long calculateDistance(Map<Coordinate, Space> image, Set<Pair> pairs, int expansion) {
        List<Integer> verticalLinesThatHaveNoGalaxies = noGalaxy(image, space -> space.position().y());
        List<Integer> horizontalLinesThatHaveNoGalaxies = noGalaxy(image, space -> space.position().x());
        long result = 0;
        for (Pair pair : pairs) {
            int startY = Math.min(pair.start.position.y, pair.end.position.y);
            int endY = Math.max(pair.start.position.y, pair.end.position.y);
            int startX = Math.min(pair.start.position.x, pair.end.position.x);
            int endX = Math.max(pair.start.position.x, pair.end.position.x);

            int yDistance = 0;
            for (int yIndex = startY; yIndex < endY; yIndex++) {
                yDistance += 1;
                if (verticalLinesThatHaveNoGalaxies.contains(yIndex)) {
                    yDistance += expansion - 1;
                }
            }

            int xDistance = 0;
            for (int xIndex = startX; xIndex < endX; xIndex++) {
                xDistance += 1;
                if (horizontalLinesThatHaveNoGalaxies.contains(xIndex)) {
                    xDistance += expansion - 1;
                }
            }

            long distance = yDistance + xDistance;
            System.out.printf("%s => %s == %s%n", pair.start.identifier, pair.end.identifier, distance);
            result += distance;
        }
        return result;
    }

    private ParseResult parse() {
        Map<Coordinate, Space> image = new HashMap<>();
        Set<Space.Galaxy> galaxies = new HashSet<>();
        Set<Pair> pairs = new HashSet<>();
        int x = 0;
        for (String line : input()) {
            List<Space> spacesForLine = new ArrayList<>();
            char[] characters = line.toCharArray();
            for (int y = 0; y < characters.length; y++) {
                Space space = parse(characters[y], x, y, galaxies.size() + 1);
                image.put(space.position(), space);
                spacesForLine.add(space);
                if (space instanceof Space.Galaxy galaxy) {
                    for (Space.Galaxy galaxy1 : galaxies) {
                        pairs.add(new Pair(galaxy1, galaxy));
                    }
                    galaxies.add(galaxy);
                }
            }
            x++;
        }
        return new ParseResult(image, pairs);
    }

    private record ParseResult(Map<Coordinate, Space> image, Set<Pair> pairs) {
    }

    List<Integer> noGalaxy(Map<Coordinate, Space> image, Function<Space, Integer> groupByFunction) {
        return image.values()
                .stream()
                .collect(Collectors.groupingBy(groupByFunction))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().stream().allMatch(space -> space instanceof Space.EmptySpace))
                .map(Map.Entry::getKey).toList();
    }

    Space parse(Character character, int x, int y, int numberOfGalaxies) {
        return switch (character) {
            case '#' -> new Space.Galaxy(String.valueOf(numberOfGalaxies), new Coordinate(x, y));
            case '.' -> new Space.EmptySpace(new Coordinate(x, y));
            default -> throw new RuntimeException("unknown character");
        };
    }

    record Pair(Space.Galaxy start, Space.Galaxy end) {

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Pair pair = (Pair) o;
            return Objects.equals(Set.of(start, end), Set.of(pair.start, pair.end));
        }

        @Override
        public int hashCode() {
            return Objects.hash(Set.of(start, end));
        }
    }

    record Coordinate(int x, int y) {
    }

    public sealed interface Space {

        Coordinate position();

        record EmptySpace(Coordinate position) implements Space {
        }

        record Galaxy(String identifier, Coordinate position) implements Space {
        }
    }
}
