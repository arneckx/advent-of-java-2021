package arneckx.year2023.day2;

import java.util.Arrays;

class Attempt {

    private final Integer redCubes;
    private final Integer greenCubes;
    private final Integer blueCubes;

    public Attempt(String partOfLine) {
        int redCubes = 0;
        int greenCubes = 0;
        int blueCubes = 0;
        String[] splittedAttempt = partOfLine.split(",");
        for (String colorWithAmount : splittedAttempt) {
            Color color =
                    Arrays.stream(Color.values()).filter(aColor -> colorWithAmount.contains(aColor.name().toLowerCase())).findFirst().orElseThrow();
            int amount = Integer.parseInt(colorWithAmount.substring(0, colorWithAmount.length() - color.name().length()).trim());

            switch (color) {
                case RED -> redCubes += amount;
                case GREEN -> greenCubes += amount;
                case BLUE -> blueCubes += amount;
            }
        }
        this.redCubes = redCubes;
        this.greenCubes = greenCubes;
        this.blueCubes = blueCubes;
    }

    public boolean isValid(GameConfiguration gameConfiguration) {
        return redCubes <= gameConfiguration.redCubes() &&
                greenCubes <= gameConfiguration.greenCubes() &&
                blueCubes <= gameConfiguration.blueCubes();
    }

    public Integer getRedCubes() {
        return redCubes;
    }

    public Integer getGreenCubes() {
        return greenCubes;
    }

    public Integer getBlueCubes() {
        return blueCubes;
    }
}
