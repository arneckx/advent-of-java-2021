package arneckx.year2023.day2;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Day2 extends Task<Integer, String> {

    private final GameConfiguration gameConfiguration = new GameConfiguration(12, 13, 14);

    public Day2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        return getGames(gameConfiguration, true).stream().mapToInt(Game::gameId).sum();
    }

    @Override
    public Integer task2() {
        return getGames(gameConfiguration, false).stream().mapToInt(Game::minimumSet).sum();
    }

    private List<Game> getGames(GameConfiguration gameConfiguration, boolean onlyValid) {
        List<Game> games = new ArrayList<>();
        for (String line : input()) {
            String[] splitted = line.split(":");
            int gameId = Integer.parseInt(splitted[0].replace("Game ", ""));
            List<Attempt> attempts = Arrays.stream(splitted[1].split(";"))
                    .map(Attempt::new)
                    .toList();

            if (onlyValid) {
                if (attempts.stream().allMatch(a -> a.isValid(gameConfiguration))) {
                    games.add(new Game(gameId, attempts));
                }
            } else {
                games.add(new Game(gameId, attempts));
            }

        }
        return games;
    }
}
