package arneckx.year2023.day2;

import java.util.List;

record Game(int gameId, List<Attempt> attempts) {

    public int minimumSet() {
        int green = attempts.stream().mapToInt(Attempt::getGreenCubes).max().orElseThrow();
        int red = attempts.stream().mapToInt(Attempt::getRedCubes).max().orElseThrow();
        int blue = attempts.stream().mapToInt(Attempt::getBlueCubes).max().orElseThrow();
        return green * red * blue;
    }
}
