package arneckx.year2023.day2;

import java.util.Arrays;

enum Color {
    RED,
    GREEN,
    BLUE;

    public static Color from(String value) {
        return Arrays.stream(Color.values())
                .filter(color -> color.name().equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("%s is not a valid color", value)));
    }
}
