package arneckx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public record InputReader<M>(Function<String, M> parser) {

    public List<M> read(String inputFile) {
        return readFile(getFile(inputFile));
    }

    private List<M> readFile(InputStream stream) {
        List<M> inputLines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                inputLines.add(parser.apply(line));
            }
            stream.close();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        return inputLines;
    }

    private InputStream getFile(String inputFile) {
        InputStream stream = InputReader.class.getResourceAsStream(inputFile);

        if (stream == null) {
            throw new IllegalArgumentException(inputFile + " is not found");
        }
        return stream;
    }
}
