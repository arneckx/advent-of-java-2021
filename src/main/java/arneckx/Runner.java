package arneckx;

import arneckx.year2022.day1.Day1;

public class Runner {

    private static final Task<?, ?> task = new Day1("/year2022/year2021/day1/input1.txt");

    private Runner() {
    }

    public static void main(String[] args) {
        System.out.println(task.task1());
    }
}
