package arneckx;

import java.util.List;
import java.util.function.Function;

public abstract class TaskDeprecated<R, M> {

    private final InputReader<M> reader = new InputReader<>(parser());
    private final String inputFile;

    protected TaskDeprecated(String inputFile) {
        this.inputFile = inputFile;
    }

    public abstract R execute();

    protected abstract Function<String, M> parser();

    protected List<M> input() {
        return reader.read(inputFile);
    }
}
