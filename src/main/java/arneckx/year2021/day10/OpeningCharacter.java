package arneckx.year2021.day10;

import java.util.Optional;

public enum OpeningCharacter {
    PARENTHESIS('(', ClosingCharacter.PARENTHESIS),
    ACCOLADE('{', ClosingCharacter.ACCOLADE),
    BRACKET('[', ClosingCharacter.BRACKET),
    BEAK('<', ClosingCharacter.BEAK);

    private final char openingCharacter;
    private final ClosingCharacter closing;

    OpeningCharacter(char openingCharacter, ClosingCharacter closing) {
        this.openingCharacter = openingCharacter;
        this.closing = closing;
    }

    public static Optional<OpeningCharacter> of(char value) {
        for (OpeningCharacter openingCharacter : OpeningCharacter.values()) {
            if (openingCharacter.openingCharacter == value) {
                return Optional.of(openingCharacter);
            }
        }
        return Optional.empty();
    }

    public ClosingCharacter closing() {
        return closing;
    }
}
