package arneckx.year2021.day10;

public class Chunk {

    private final OpeningCharacter openingCharacter;
    private ClosingCharacter closingCharacter;
    private boolean isCorrupt = false;

    public Chunk(char openingCharacter) {
        this.openingCharacter = OpeningCharacter.of(openingCharacter).orElseThrow();
    }

    public Chunk closingCharacter(char character) {
        closingCharacter = ClosingCharacter.of(character).orElseThrow();
        if (closingCharacter != openingCharacter.closing()) {
            System.out.println(String.format("Expected %s, but found %S instead", openingCharacter.closing().closingCharacter(),
                    closingCharacter().closingCharacter()));
            isCorrupt = true;
        }
        return this;
    }

    public OpeningCharacter openingCharacter() {
        return openingCharacter;
    }

    public ClosingCharacter closingCharacter() {
        return closingCharacter;
    }

    public boolean isCorrupt() {
        return isCorrupt;
    }
}
