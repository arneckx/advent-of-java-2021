package arneckx.year2021.day10;

import java.util.Optional;

public enum ClosingCharacter {
    PARENTHESIS(')', 3, 1),
    ACCOLADE('}', 1197, 3),
    BRACKET(']', 57, 2),
    BEAK('>', 25137, 4);

    private final char closingCharacter;
    private final int score;
    private final int complementScore;

    ClosingCharacter(char closingCharacter, int score, int complementScore) {
        this.closingCharacter = closingCharacter;
        this.score = score;
        this.complementScore = complementScore;
    }

    public static Optional<ClosingCharacter> of(char value) {
        for (ClosingCharacter closingCharacter : ClosingCharacter.values()) {
            if (closingCharacter.closingCharacter == value) {
                return Optional.of(closingCharacter);
            }
        }
        return Optional.empty();
    }

    public int score() {
        return score;
    }

    public char closingCharacter() {
        return closingCharacter;
    }

    public int complementScore() {
        return complementScore;
    }
}
