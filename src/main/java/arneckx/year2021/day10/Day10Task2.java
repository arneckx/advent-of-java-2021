package arneckx.year2021.day10;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day10Task2 extends TaskDeprecated<Long, String> {

    private List<Chunk> chunks = new ArrayList<>();

    public Day10Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long execute() {
        List<Long> collectNumbers = input()
                .stream()
                .map(this::createChunks)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        Collections.sort(collectNumbers);

        int middle = collectNumbers.size() / 2;
        Long result = collectNumbers.get(middle);

        System.out.println(result);
        return result;
    }

    public Optional<Long> createChunks(String line) {
        chunks = new ArrayList<>();
        for (char character : line.toCharArray()) {
            if (isOpening(character)) {
                chunks.add(new Chunk(character));
            }
            if (isClosing(character)) {
                Chunk lastChunk = chunks.get(chunks.size() - 1);
                if (lastChunk.closingCharacter(character).isCorrupt()) {
                    return Optional.empty();
                }
                chunks.remove(chunks.size() - 1);
            }
        }
        if (chunks.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(calculateMissingCharactersScore());
    }

    private long calculateMissingCharactersScore() {
        long score = 0;
        List<ClosingCharacter> missingCharacters = new ArrayList<>();
        Collections.reverse(chunks);
        for (Chunk chunk : chunks) {
            missingCharacters.add(chunk.openingCharacter().closing());
        }

        for (ClosingCharacter missingCharacter : missingCharacters) {
            score = score * 5;
            score += missingCharacter.complementScore();
        }

        return score;
    }

    private boolean isOpening(char character) {
        return OpeningCharacter.of(character).isPresent();
    }

    private boolean isClosing(char character) {
        return ClosingCharacter.of(character).isPresent();
    }
}
