package arneckx.year2021.day10;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day10Task1 extends TaskDeprecated<Integer, String> {

    private List<Chunk> chunks = new ArrayList<>();

    public Day10Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        int totalSum = input()
                .stream()
                .map(this::createChunks)
                .mapToInt(score -> score)
                .sum();
        System.out.println(totalSum);
        return totalSum;
    }

    public int createChunks(String line) {
        chunks = new ArrayList<>();
        for (char character : line.toCharArray()) {
            if (isOpening(character)) {
                chunks.add(new Chunk(character));
            }
            if (isClosing(character)) {
                Chunk lastChunk = chunks.get(chunks.size() - 1);
                if (lastChunk.closingCharacter(character).isCorrupt()) {
                    return lastChunk.closingCharacter().score();
                }
                chunks.remove(chunks.size() - 1);
            }
        }
        return 0;
    }

    private boolean isOpening(char character) {
        return OpeningCharacter.of(character).isPresent();
    }

    private boolean isClosing(char character) {
        return ClosingCharacter.of(character).isPresent();
    }
}
