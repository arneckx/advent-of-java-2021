package arneckx.year2021.day11;

import java.util.List;

public record Coordinate(int row, int column) {

    public List<Coordinate> around() {
        return List.of(
                // vertical
                new Coordinate(row - 1, column),
                new Coordinate(row + 1, column),
                // horizontal
                new Coordinate(row, column - 1),
                new Coordinate(row, column + 1),
                // diagonal
                new Coordinate(row - 1, column - 1),
                new Coordinate(row - 1, column + 1),
                new Coordinate(row + 1, column - 1),
                new Coordinate(row + 1, column + 1)
        );
    }
}
