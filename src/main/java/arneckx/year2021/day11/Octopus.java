package arneckx.year2021.day11;

public class Octopus {
    private final Coordinate coordinate;
    private int energyLevel;
    private boolean hasFlashed;

    public Octopus(Coordinate coordinate, int energyLevel) {
        this.coordinate = coordinate;
        this.energyLevel = energyLevel;
    }

    public void increaseEnergyByOne() {
        energyLevel++;
    }

    public void reset() {
        hasFlashed = false;
        if (energyLevel > 9) {
            energyLevel = 0;
        }
    }

    public Coordinate coordinate() {
        return coordinate;
    }

    public int energyLevel() {
        return energyLevel;
    }

    public boolean hasFlashed() {
        return hasFlashed;
    }

    public void flash() {
        hasFlashed = true;
    }
}
