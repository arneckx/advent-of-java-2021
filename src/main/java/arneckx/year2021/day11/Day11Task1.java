package arneckx.year2021.day11;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class Day11Task1 extends TaskDeprecated<Integer, String> {

    private final HashMap<Coordinate, Octopus> octopuses = new HashMap<>();
    private static final int STEPS = 100;
    private int totalFLashes = 0;

    public Day11Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapOctopuses();

        for (int step = 0; step < STEPS; step++) {
            octopuses.values().forEach(Octopus::increaseEnergyByOne);
            octopuses.values().forEach(this::checkForFlash);
            octopuses.values().forEach(Octopus::reset);
        }

        return totalFLashes;
    }

    private void checkForFlash(Octopus octopus) {
        if (octopus.energyLevel() > 9 && !octopus.hasFlashed()) {
            octopus.flash();
            totalFLashes++;
            octopus.coordinate().around().forEach(this::increaseEnergyAround);
        }
    }

    private void increaseEnergyAround(Coordinate coordinate) {
        List<Octopus> octopusListAround = new ArrayList<>();
        if (octopuses.containsKey(coordinate)) {
            octopusListAround.add(octopuses.get(coordinate));
        }
        octopusListAround.forEach(Octopus::increaseEnergyByOne);
        octopusListAround.forEach(this::checkForFlash);
    }

    private void mapOctopuses() {
        List<String> input = input();
        for (int row = 0; row < input.size(); row++) {
            char[] rowNumbers = input.get(row).toCharArray();
            for (int column = 0; column < rowNumbers.length; column++) {
                int energyLevel = Integer.parseInt(String.valueOf(rowNumbers[column]));
                Coordinate coordinate = new Coordinate(row, column);
                octopuses.put(coordinate, new Octopus(coordinate, energyLevel));
            }
        }
    }
}
