package arneckx.year2021.day3;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day3Task1 extends TaskDeprecated<Integer, Reading> {

    public Day3Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Reading> parser() {
        return Reading::map;
    }

    @Override
    public Integer execute() {
        DiagnosticReport diagnosticReport = new DiagnosticReport(input());

        int gamma = diagnosticReport.gamma();
        int epsilon = diagnosticReport.epsilon();
        int power = gamma * epsilon;

        System.out.printf("gamma rate: %s epsilon rate: %s power consumption %s", gamma, epsilon, power);
        return power;
    }
}
