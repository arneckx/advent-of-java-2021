package arneckx.year2021.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class BinaryNumber {
    private final int index;
    private int zeroCount;
    private int oneCount;
    private final List<Reading> oneReadings = new ArrayList<>();
    private final List<Reading> zeroReadings = new ArrayList<>();

    public BinaryNumber(int index) {
        this.index = index;
    }

    public void addOneFor(Reading reading) {
        oneCount++;
        oneReadings.add(reading);
    }

    public void addZeroFor(Reading reading) {
        zeroCount++;
        zeroReadings.add(reading);
    }

    public int moreFrequentBit() {
        if (zeroCount > oneCount) {
            return 0;
        } else {
            return 1;
        }
    }

    public int lessFrequentBit() {
        if (zeroCount > oneCount) {
            return 1;
        } else {
            return 0;
        }
    }

    public List<Reading> moreFrequentReadings() {
        if (zeroCount > oneCount) {
            return zeroReadings;
        } else {
            return oneReadings;
        }
    }

    public List<Reading> lessFrequentReadings() {
        if (zeroCount > oneCount) {
            return oneReadings;
        } else {
            return zeroReadings;
        }
    }

    public List<Reading> moreFrequentReadings(List<Reading> readings) {
        List<Reading> readings0 = zeroReadings
                .stream()
                .filter(readings::contains)
                .collect(Collectors.toList());
        List<Reading> readings1 = oneReadings
                .stream()
                .filter(readings::contains)
                .collect(Collectors.toList());

        if (readings0.size() > readings1.size()) {
            return readings0;
        } else {
            return readings1;
        }
    }

    public List<Reading> lessFrequentReadings(List<Reading> readings) {
        List<Reading> readings0 = zeroReadings
                .stream()
                .filter(readings::contains)
                .collect(Collectors.toList());
        List<Reading> readings1 = oneReadings
                .stream()
                .filter(readings::contains)
                .collect(Collectors.toList());

        if (readings0.size() > readings1.size()) {
            return readings1;
        } else {
            return readings0;
        }
    }
}
