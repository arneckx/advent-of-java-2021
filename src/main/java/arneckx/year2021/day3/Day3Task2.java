package arneckx.year2021.day3;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day3Task2 extends TaskDeprecated<Integer, Reading> {

    public Day3Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Reading> parser() {
        return Reading::map;
    }

    @Override
    public Integer execute() {
        DiagnosticReport diagnosticReport = new DiagnosticReport(input());

        int oxygen = diagnosticReport.oxygen();
        int co2 = diagnosticReport.co2();
        int lifeSupportRating = oxygen * co2;

        System.out.printf("oxygen generator rating: %s CO2 scrubber rating: %s life support rating %s", oxygen, co2, lifeSupportRating);
        return lifeSupportRating;
    }
}
