package arneckx.year2021.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Reading {

    private final List<Integer> bitArray = new ArrayList<>();

    public static Reading map(String line) {
        Reading bit = new Reading();
        for (int index = 0; index < line.toCharArray().length; index++) {
            bit.add(index, Integer.parseInt(String.valueOf(line.toCharArray()[index])));
        }
        return bit;
    }

    private void add(int index, int bit) {
        bitArray.add(index, bit);
    }

    public List<Integer> bitArray() {
        return bitArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reading reading = (Reading) o;
        return Objects.equals(bitArray, reading.bitArray);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bitArray);
    }

    @Override
    public String toString() {
        return bitArray.stream()
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
