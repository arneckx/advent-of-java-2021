package arneckx.year2021.day3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class DiagnosticReport {

    private final HashMap<Integer, BinaryNumber> report = new HashMap<>();

    public DiagnosticReport(List<Reading> readings) {
        readings.forEach(reading -> {
            for (int index = 0; index < reading.bitArray().size(); index++) {
                addRecord(index, reading.bitArray().get(index), reading);
            }
        });
    }

    public void addRecord(int index, int bit, Reading reading) {
        BinaryNumber binaryNumber = getBinaryNumber(index);

        if (bit == 0) {
            binaryNumber.addZeroFor(reading);
        } else {
            binaryNumber.addOneFor(reading);
        }

        binaryNumber(index, binaryNumber);
    }

    private void binaryNumber(int index, BinaryNumber binaryNumber) {
        report.put(index, binaryNumber);
    }

    private BinaryNumber getBinaryNumber(int index) {
        BinaryNumber binaryNumber;
        if (report.containsKey(index)) {
            binaryNumber = report.get(index);
        } else {
            binaryNumber = new BinaryNumber(index);
        }
        return binaryNumber;
    }

    public int gamma() {
        StringBuilder binaryString = new StringBuilder();
        for (BinaryNumber binaryNumber : report.values()) {
            binaryString.append(binaryNumber.moreFrequentBit());
        }
        return Integer.parseInt(binaryString.toString(), 2);
    }

    public int epsilon() {
        StringBuilder binaryString = new StringBuilder();
        for (BinaryNumber binaryNumber : report.values()) {
            binaryString.append(binaryNumber.lessFrequentBit());
        }
        return Integer.parseInt(binaryString.toString(), 2);
    }

    public int oxygen() {
        List<Reading> oxygenReadings = new ArrayList<>();
        for (BinaryNumber binaryNumber : report.values()) {
            if (oxygenReadings.size() != 1) {
                if (oxygenReadings.isEmpty()) {
                    oxygenReadings.addAll(binaryNumber.moreFrequentReadings());
                } else {
                    List<Reading> moreFrequentReadings = binaryNumber.moreFrequentReadings(oxygenReadings);
                    List<Reading> newOxygenReadings = oxygenReadings.stream()
                            .filter(moreFrequentReadings::contains)
                            .collect(Collectors.toList());
                    oxygenReadings = newOxygenReadings;
                }
            }
        }
        if (oxygenReadings.size() != 1) {
            throw new RuntimeException("Reading size is not start");
        }
        return Integer.parseInt(oxygenReadings.get(0).toString(), 2);
    }

    public int co2() {
        List<Reading> co2Readings = new ArrayList<>();
        for (BinaryNumber binaryNumber : report.values()) {
            if (co2Readings.size() != 1) {
                if (co2Readings.isEmpty()) {
                    co2Readings.addAll(binaryNumber.lessFrequentReadings());
                } else {
                    List<Reading> lessFrequentReadings = binaryNumber.lessFrequentReadings(co2Readings);
                    List<Reading> newCo2Readings = co2Readings.stream()
                            .filter(lessFrequentReadings::contains)
                            .collect(Collectors.toList());
                    co2Readings = newCo2Readings;
                }
            }
        }
        if (co2Readings.size() != 1) {
            throw new RuntimeException("Reading size is not start");
        }
        return Integer.parseInt(co2Readings.get(0).toString(), 2);
    }
}
