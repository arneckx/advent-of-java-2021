package arneckx.year2021.day2;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day2Task2 extends TaskDeprecated<Integer, Command> {

    private int horizontalPosition;
    private int depth;
    private int aim;

    public Day2Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Command> parser() {
        return Command::map;
    }

    @Override
    public Integer execute() {
        input().forEach(this::move);

        System.out.printf("horizontal position: %s | depth: %s%n", horizontalPosition, depth);
        return horizontalPosition * depth;
    }

    private void move(Command command) {
        switch (command.bearing()) {
            case FORWARD -> goForward(command);
            case DOWN -> aim += command.amount();
            case UP -> aim -= command.amount();
            default -> throw new RuntimeException("unknown bearing");
        }
    }

    private void goForward(Command command) {
        horizontalPosition += command.amount();
        depth += (aim * command.amount());
    }
}
