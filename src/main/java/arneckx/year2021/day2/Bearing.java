package arneckx.year2021.day2;

public enum Bearing {
    FORWARD,
    DOWN,
    UP;

    public static Bearing fromValue(String name) {
        return switch (name) {
            case "forward" -> FORWARD;
            case "down" -> DOWN;
            case "up" -> UP;
            default -> throw new RuntimeException("unknown bearing");
        };
    }
}
