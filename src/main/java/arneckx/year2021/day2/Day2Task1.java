package arneckx.year2021.day2;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day2Task1 extends TaskDeprecated<Integer, Command> {

    private int horizontalPosition;
    private int depth;

    public Day2Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Command> parser() {
        return Command::map;
    }

    @Override
    public Integer execute() {
        for (Command command : input()) {
            move(command);
        }

        System.out.printf("horizontal position: %s | depth: %s%n", horizontalPosition, depth);
        return horizontalPosition * depth;
    }

    private void move(Command command) {
        switch (command.bearing()) {
            case FORWARD -> horizontalPosition += command.amount();
            case DOWN -> depth += command.amount();
            case UP -> depth -= command.amount();
            default -> throw new RuntimeException("unknown bearing");
        }
    }
}
