package arneckx.year2021.day2;

public record Command(Bearing bearing, int amount) {

    public static Command map(String line) {
        String[] split = line.split(" ");
        return new Command(Bearing.fromValue(split[0]), Integer.parseInt(split[1]));
    }
}
