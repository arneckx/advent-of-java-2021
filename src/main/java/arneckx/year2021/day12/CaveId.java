package arneckx.year2021.day12;

public record CaveId(String caveId) {

    public boolean isUppercase() {
        return Character.isUpperCase(caveId.charAt(0));
    }

    public static CaveId of(String id) {
        return new CaveId(id);
    }

    @Override
    public String toString() {
        return caveId;
    }
}
