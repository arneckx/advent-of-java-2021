package arneckx.year2021.day12;

import arneckx.TaskDeprecated;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public class Day12Task1 extends TaskDeprecated<Integer, String> {

    private final HashMap<CaveId, Cave> caves = new HashMap<>();
    private final Set<Path> resultPaths = new HashSet<>();

    public Day12Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapInputToCaves();

        Cave start = caves.get(CaveId.of("start"));
        caves.remove(start.id());
        Path path = new Path().add(start);

        calculatePaths(new Path(path), start, new HashMap<>(caves));

        return resultPaths.size();
    }

    public void calculatePaths(Path path, Cave currentCave, Map<CaveId, Cave> caves) {
        for (Cave cave : currentCave.connectedCaves()) {
            Map<CaveId, Cave> copyOfCaves = new HashMap<>(caves);
            if (copyOfCaves.containsKey(cave.id())) {
                if (cave.isSmall()) {
                    copyOfCaves.remove(cave.id());
                }
                Path copyOfPath = new Path(path);
                copyOfPath.add(cave);
                if (cave.id().equals(CaveId.of("end"))) {
                    resultPaths.add(copyOfPath);
                }
                calculatePaths(copyOfPath, new Cave(cave), new HashMap<>(copyOfCaves));
            }
        }
    }

    private void mapInputToCaves() {
        for (String line : input()) {
            String[] splittedLine = line.split("-");
            connectCaves(splittedLine[0], splittedLine[1]);
        }
    }

    private void connectCaves(String fromCaveString, String toCaveString) {
        CaveId fromCaveId = CaveId.of(fromCaveString);
        CaveId toCaveId = CaveId.of(toCaveString);
        Cave fromCave = Optional.ofNullable(caves.get(fromCaveId)).orElse(new Cave(fromCaveId));
        Cave toCave = Optional.ofNullable(caves.get(toCaveId)).orElse(new Cave(toCaveId));

        fromCave.connect(toCave);
        toCave.connect(fromCave);

        caves.put(fromCaveId, fromCave);
        caves.put(toCaveId, toCave);
    }
}
