package arneckx.year2021.day12;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Path {

    private final List<Cave> caves;

    public Path() {
        caves = new ArrayList<>();
    }

    public Path(Path path) {
        caves = new ArrayList<>(path.caves);
    }

    public Path add(Cave cave) {
        caves.add(new Cave(cave));
        return this;
    }

    public boolean containsSmallCaveMoreThanOnce(Cave cave) {
        return containsAnySmallCaveThatOccursMoreThanOnce() && smallCaveAlreadyOccurs(cave);
    }

    private boolean containsAnySmallCaveThatOccursMoreThanOnce() {
        return this.caves.stream()
                .filter(Cave::isSmall)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .anyMatch(count -> count.getValue() >= 2);
    }

    private boolean smallCaveAlreadyOccurs(Cave cave) {
        List<Cave> cavesThaOccurMoreThanOnce = this.caves.stream()
                .filter(Cave::isSmall)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(count -> count.getValue() >= 1)
                .map(Map.Entry::getKey)
                .toList();

        return cavesThaOccurMoreThanOnce.contains(cave);
    }

    public List<Cave> caves() {
        return caves;
    }

    public Cave last() {
        return this.caves().get(caves().size() - 1);
    }

    public Cave first() {
        return this.caves().get(0);
    }

    @Override
    public String toString() {
        return caves.stream()
                .map(cave -> cave.id().caveId())
                .collect(Collectors.joining(","));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Path path = (Path) o;
        return Objects.equals(caves, path.caves);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caves);
    }
}
