package arneckx.year2021.day12;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Cave {

    private final boolean isBig;
    private final CaveId id;
    private List<Cave> connectedCaves = new ArrayList<>();

    public Cave(CaveId id) {
        this.id = id;
        this.isBig = id().isUppercase();
    }

    public Cave(Cave cave) {
        this(cave.id());
        connectedCaves = new ArrayList<>(cave.connectedCaves());
    }

    public void connect(Cave toCave) {
        connectedCaves.add(toCave);
    }

    public boolean isBig() {
        return isBig;
    }

    public boolean isSmall() {
        return !isBig();
    }

    public CaveId id() {
        return id;
    }

    public List<Cave> connectedCaves() {
        return connectedCaves;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cave cave = (Cave) o;
        return Objects.equals(id, cave.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
