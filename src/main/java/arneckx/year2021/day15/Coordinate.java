package arneckx.year2021.day15;

import java.util.List;

public record Coordinate(int row, int column) {

    public List<Coordinate> around() {
        return List.of(
                new Coordinate(row, column + 1),
                new Coordinate(row, column - 1),
                new Coordinate(row + 1, column),
                new Coordinate(row - 1, column)
        );
    }

    public Coordinate expand(int maxRow, int maxColumn) {
        return new Coordinate(maxRow + this.row, this.column + maxColumn);
    }
}
