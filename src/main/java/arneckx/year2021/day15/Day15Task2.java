package arneckx.year2021.day15;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public class Day15Task2 extends TaskDeprecated<Integer, String> {

    private final Map<Coordinate, Position> inititalTile = new HashMap<>();
    private final Map<Coordinate, Position> fullMap = new HashMap<>();
    private Coordinate endCoordinate;
    private Coordinate initialEndCoordinate;

    public Day15Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapLocations();
        createFullMap();

        Position start = fullMap.get(new Coordinate(0, 0));
        start.riskLevel(0);

        fullMap.remove(new Coordinate(0, 0));

        calculateShortestPathFromSource(start, new HashMap<>(fullMap));
        Position end = fullMap.get(endCoordinate);

        System.out.println(end.distance());
        return end.distance();
    }

    private void createFullMap() {
        fullMap.putAll(inititalTile);

        expandToTheRight();

        int maxRow = endCoordinate.row();
        for (int step = 0; step < 4; step++) {
            for (int row = 0; row <= endCoordinate.row(); row++) {
                for (int column = 0; column <= endCoordinate.column(); column++) {
                    Position current = fullMap.get(new Coordinate(row, column));
                    int nextRow = maxRow + current.coordinate().row() + 1;
                    Coordinate nextCoordinate = new Coordinate(nextRow, current.coordinate().column());
                    int nextRiskLevel = current.nextRiskLevel(step);
                    fullMap.put(nextCoordinate, new Position(nextRiskLevel, nextCoordinate));
                }
            }
            maxRow += endCoordinate.row() + 1;
        }
        endCoordinate = new Coordinate(maxRow, endCoordinate.column());
    }

    private void expandToTheRight() {
        int maxColumn = endCoordinate.column();
        for (int step = 0; step < 4; step++) {
            for (int row = 0; row <= endCoordinate.row(); row++) {
                for (int column = 0; column <= endCoordinate.column(); column++) {
                    Position current = inititalTile.get(new Coordinate(row, column));
                    int nextColumn = maxColumn + current.coordinate().column() + 1;
                    Coordinate nextCoordinate = new Coordinate(current.coordinate().row(), nextColumn);
                    int nextRiskLevel = current.nextRiskLevel(step);
                    fullMap.put(nextCoordinate, new Position(nextRiskLevel, nextCoordinate));
                }
            }
            maxColumn += endCoordinate.column() + 1;
        }
        endCoordinate = new Coordinate(endCoordinate.row(), maxColumn);
    }

    public void calculateShortestPathFromSource(Position start, Map<Coordinate, Position> positionsMap) {
        start.distance(0);

        Set<Position> settledPositions = new HashSet<>();
        Set<Position> unsettledPositions = new HashSet<>();

        unsettledPositions.add(start);

        while (!unsettledPositions.isEmpty()) {
            Position currentPosition = getLowestDistanceNode(unsettledPositions);
            unsettledPositions.remove(currentPosition);

            for (Position adjacentPosition : getAdjacentPositions(positionsMap, currentPosition)) {
                if (!settledPositions.contains(adjacentPosition)) {
                    calculateMinimumDistance(adjacentPosition, adjacentPosition.riskLevel(), currentPosition);
                    unsettledPositions.add(adjacentPosition);
                }
            }
            settledPositions.add(currentPosition);
        }
    }

    private List<Position> getAdjacentPositions(Map<Coordinate, Position> positionsMap, Position currentPosition) {
        List<Position> adjacentPositions = new ArrayList<>();
        for (Coordinate coordinateAround : currentPosition.coordinate().around()) {
            if (positionsMap.containsKey(coordinateAround)) {
                adjacentPositions.add(positionsMap.get(coordinateAround));
            }
        }

        return adjacentPositions;
    }

    private static Position getLowestDistanceNode(Set<Position> unsettledNodes) {
        Position lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Position position : unsettledNodes) {
            int nodeDistance = position.distance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = position;
            }
        }
        return lowestDistanceNode;
    }

    private void calculateMinimumDistance(Position evaluationNode, int edgeWeigh, Position sourceNode) {
        int sourceDistance = sourceNode.distance();
        if (sourceDistance + edgeWeigh <= evaluationNode.distance()) {
            evaluationNode.distance(sourceDistance + edgeWeigh);
            LinkedList<Position> shortestPath = new LinkedList<>(sourceNode.shortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.shortestPath(shortestPath);
        }
    }

    private void mapLocations() {
        int maxRow = 0;
        int maxColumn = 0;
        List<String> input = input();
        for (int row = 0; row < input.size(); row++) {
            char[] rowNumbers = input.get(row).toCharArray();
            for (int column = 0; column < rowNumbers.length; column++) {
                int riskLevel = Integer.parseInt(String.valueOf(rowNumbers[column]));
                Coordinate coordinate = new Coordinate(row, column);
                inititalTile.put(coordinate, new Position(riskLevel, coordinate));
                if (row > maxRow) {
                    maxRow = row;
                }
                if (column > maxColumn) {
                    maxColumn = column;
                }
            }
        }

        endCoordinate = new Coordinate(maxRow, maxColumn);
        initialEndCoordinate = new Coordinate(maxRow, maxColumn);
    }
}
