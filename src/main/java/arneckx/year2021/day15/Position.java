package arneckx.year2021.day15;

import java.util.LinkedList;
import java.util.List;

public class Position {
    private int riskLevel;
    private Coordinate coordinate;
    private Integer distance = Integer.MAX_VALUE;
    private List<Position> shortestPath = new LinkedList<>();

    public Position(int riskLevel, Coordinate coordinate) {
        this.riskLevel = riskLevel;
        this.coordinate = coordinate;
    }

    public Position distance(Integer distance) {
        this.distance = distance;
        return this;
    }

    public Position riskLevel(int riskLevel) {
        this.riskLevel = riskLevel;
        return this;
    }

    public Integer distance() {
        return distance;
    }

    public Position shortestPath(List<Position> shortestPath) {
        this.shortestPath = shortestPath;
        return this;
    }

    public List<Position> shortestPath() {
        return shortestPath;
    }

    public int riskLevel() {
        return riskLevel;
    }

    public Coordinate coordinate() {
        return coordinate;
    }

    public int nextRiskLevel(int step) {
        int newRiskLevel = this.riskLevel;
        for (int count = 0; count <= step; count++) {
            if (newRiskLevel == 9) {
                newRiskLevel = 1;
            } else {
                newRiskLevel += 1;
            }
        }

        return newRiskLevel;
    }

    @Override
    public String toString() {
        return coordinate.toString();
    }
}
