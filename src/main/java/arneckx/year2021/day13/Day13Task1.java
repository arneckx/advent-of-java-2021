package arneckx.year2021.day13;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Day13Task1 extends TaskDeprecated<Integer, String> {

    private final Set<Coordinate> coordinates = new HashSet<>();
    private final List<Fold> folds = new ArrayList<>();

    public Day13Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapInput();

        Page page = new Page(coordinates);

        page.fold(folds.get(0));

        return page.dots();
    }

    private void mapInput() {
        List<String> input = input();

        int index = 0;
        String line = input.get(index);
        while (!line.isBlank()) {
            String[] split = line.split(",");
            coordinates.add(new Coordinate(
                    Integer.parseInt(split[1].trim()),
                    Integer.parseInt(split[0].trim())
            ));
            index++;
            line = input.get(index);
        }

        for (int secondIndex = ++index; secondIndex < input.size(); secondIndex++) {
            line = input.get(secondIndex);
            String[] split = line.replace("fold along", "").trim().split("=");
            folds.add(new Fold(split[0], Integer.parseInt(split[1])));
        }
    }
}
