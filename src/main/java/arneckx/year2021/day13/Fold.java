package arneckx.year2021.day13;

public record Fold(String foldOn, int foldNumber) {

    public Coordinate fold(Coordinate coordinate) {
        if ("y".equals(foldOn)) {
            if (coordinate.row() < foldNumber) {
                return coordinate;
            } else {
                return new Coordinate(foldNumber * 2 - coordinate.row(), coordinate.column());
            }
        }
        if ("x".equals(foldOn)) {
            if (coordinate.column() < foldNumber) {
                return coordinate;
            } else {
                return new Coordinate(coordinate.row(), foldNumber * 2 - coordinate.column());
            }
        }
        throw new RuntimeException("not foldable");
    }
}
