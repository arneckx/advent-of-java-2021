package arneckx.year2021.day13;

public record Coordinate(int row, int column) {
}
