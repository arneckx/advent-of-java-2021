package arneckx.year2021.day13;

import java.util.Set;
import java.util.stream.Collectors;

public class Page {
    private Set<Coordinate> coordinates;

    public Page(Set<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    public void fold(Fold fold) {
        Set<Coordinate> newCoordinates = this.coordinates
                .stream()
                .map(fold::fold)
                .collect(Collectors.toSet());

        coordinates = newCoordinates;
    }

    public int dots() {
        return coordinates.size();
    }

    private int highestColumn() {
        return coordinates.stream()
                .mapToInt(Coordinate::column)
                .max()
                .orElseThrow();
    }

    private int highestRow() {
        return coordinates.stream()
                .mapToInt(Coordinate::row)
                .max()
                .orElseThrow();
    }

    public void print() {
        StringBuilder string = new StringBuilder();
        for (int row = 0; row <= highestRow(); row++) {
            for (int column = 0; column <= highestColumn(); column++) {
                if (coordinates.contains(new Coordinate(row, column))) {
                    string.append("# ");
                } else {
                    string.append("  ");
                }
            }
            string.append("\n");
        }

        System.out.println(string);
    }
}
