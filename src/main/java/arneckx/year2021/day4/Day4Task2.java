package arneckx.year2021.day4;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day4Task2 extends TaskDeprecated<Integer, String> {

    public Day4Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        BingoGame bingoGame = new BingoGame(input());

        Board winningBoard = bingoGame.playDirty();

        bingoGame.boards().forEach(System.out::println);

        int result = winningBoard.sumOfAllUnmarkedNumbers() * bingoGame.currentNumber();
        System.out.println("last number was: " + bingoGame.currentNumber());
        return result;
    }
}
