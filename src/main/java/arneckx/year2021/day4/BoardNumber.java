package arneckx.year2021.day4;

public class BoardNumber {
    private final int row;
    private final int column;
    private final int number;
    private boolean isMarked = false;

    public BoardNumber(int row, int column, int number) {
        this.row = row;
        this.column = column;
        this.number = number;
    }

    public void mark() {
        isMarked = true;
    }

    public int row() {
        return row;
    }

    public int column() {
        return column;
    }

    public int number() {
        return number;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public boolean isNotMarked() {
        return !isMarked;
    }
}
