package arneckx.year2021.day4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Board {
    private final HashMap<Integer, BoardNumber> numbers = new HashMap<>();
    private Map<Integer, List<BoardNumber>> numbersForRow = new HashMap<>();
    private Map<Integer, List<BoardNumber>> numbersForColumn = new HashMap<>();

    private int column = 1;
    private int row = 1;

    public void addRow(String line) {
        if (row > 5) {
            throw new RuntimeException("Too many rows");
        }
        for (String numberAsString : line.trim().split(" ")) {
            if (!numberAsString.isBlank()) {
                addNumber(Integer.parseInt(numberAsString.trim()));
                column++;
            }
        }
        row++;
        column = 1;
    }

    private void addNumber(int number) {
        if (column > 5) {
            throw new RuntimeException("Too many rows");
        }
        BoardNumber boardNumber = new BoardNumber(row, column, number);
        numbers.put(number, boardNumber);

        List<BoardNumber> rowNumbers = Optional.ofNullable(numbersForRow.get(row)).orElse(new ArrayList<>());
        rowNumbers.add(boardNumber);
        numbersForRow.put(row, rowNumbers);

        List<BoardNumber> columnNumbers = Optional.ofNullable(numbersForColumn.get(column)).orElse(new ArrayList<>());
        columnNumbers.add(boardNumber);
        numbersForColumn.put(column, columnNumbers);
    }

    public boolean markNumber(int currentNumber) {
        if (numbers.containsKey(currentNumber)) {
            numbers.get(currentNumber).mark();
            return checkForBingo();
        }
        return false;
    }

    private boolean checkForBingo() {
        return containsBingo(numbersForRow.values()) ||
                containsBingo(numbersForColumn.values());
    }

    private boolean containsBingo(Collection<List<BoardNumber>> values) {
        return values.stream()
                .map(this::areAllMarked)
                .filter(areAllMarked -> areAllMarked)
                .findAny()
                .orElse(false);
    }

    private boolean areAllMarked(List<BoardNumber> listOfNumbers) {
        List<BoardNumber> markedNumbers = listOfNumbers
                .stream()
                .filter(BoardNumber::isMarked)
                .collect(Collectors.toList());
        return markedNumbers.size() == listOfNumbers.size();
    }

    public int sumOfAllUnmarkedNumbers() {
        return numbers
                .values()
                .stream()
                .filter(BoardNumber::isNotMarked)
                .map(BoardNumber::number)
                .mapToInt(number -> number)
                .sum();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer, List<BoardNumber>> rowNumbers : numbersForRow.entrySet()) {
            for (BoardNumber boardNumber : rowNumbers.getValue()) {
                if (boardNumber.isMarked()) {
                    builder.append(String.format("x%s\t", boardNumber.number()));
                } else {
                    builder.append(String.format("%s \t", boardNumber.number()));
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }
}
