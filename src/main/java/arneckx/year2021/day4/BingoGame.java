package arneckx.year2021.day4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BingoGame {

    private final List<Integer> numbers;
    private final List<Board> boards = new ArrayList<>();
    private int counter = -1;

    public BingoGame(List<String> input) {
        numbers = firstLineNumbers(input.get(0));

        input.remove(0);
        input.remove(0);

        Board board = new Board();
        for (String line : input) {
            if (empty(line)) {
                boards.add(board);
                board = new Board();
            } else {
                board.addRow(line);
            }
        }
        boards.add(board);
    }

    public Board play() {
        boolean hasBingo = false;
        while (!hasBingo) {
            nextRound();
            for (Board board : boards()) {
                hasBingo = board.markNumber(currentNumber());
                if (hasBingo) {
                    return board;
                }
            }
        }
        throw new RuntimeException("no winner");
    }

    public Board playDirty() {
        List<Board> winningBoards = new ArrayList<>();
        boolean hasBingo = false;
        while (true) {
            nextRound();
            for (Board board : boards()) {
                if (winningBoards.contains(board)) {
                    // ignore
                } else {
                    hasBingo = board.markNumber(currentNumber());
                    if (hasBingo) {
                        if (isLastBoard(winningBoards)) {
                            return board;
                        } else {
                            winningBoards.add(board);
                        }
                    }
                }
            }
        }
        //        throw new RuntimeException("no winner");
    }

    private boolean isLastBoard(List<Board> winningBoards) {
        return winningBoards.size() == boards().size() - 1;
    }

    public void nextRound() {
        counter++;
    }

    public int currentNumber() {
        return numbers.get(counter);
    }

    private boolean empty(String line) {
        return line.isEmpty() || line.isBlank();
    }

    private List<Integer> firstLineNumbers(String commaSeperatedNumbers) {
        return Arrays.stream(commaSeperatedNumbers
                        .split(","))
                .map(Integer::parseInt)
                .toList();
    }

    public List<Board> boards() {
        return boards;
    }
}
