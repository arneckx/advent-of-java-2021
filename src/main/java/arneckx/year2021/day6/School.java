package arneckx.year2021.day6;

import java.util.HashMap;
import java.util.Map;

public class School {
    private final Map<LanternFishEnum, Long> fishByCount;

    public School(Map<LanternFishEnum, Long> fishByCount) {
        this.fishByCount = fishByCount;
    }

    public School() {
        this.fishByCount = new HashMap<>();
    }

    public Map<LanternFishEnum, Long> fish() {
        return fishByCount;
    }

    public void add(LanternFishEnum lanternFish, Long amountOfFish) {
        if (fishByCount.containsKey(lanternFish)) {
            Long currentFishCount = fishByCount.get(lanternFish);
            fishByCount.put(lanternFish, currentFishCount + amountOfFish);
        } else {
            fishByCount.put(lanternFish, amountOfFish);
        }
    }

    public void add(Map.Entry<LanternFishEnum, Long> fishByCount) {
        switch (fishByCount.getKey()) {
            case ZERO -> {
                add(LanternFishEnum.SIX, fishByCount.getValue());
                add(LanternFishEnum.EIGHT, fishByCount.getValue());
            }
            default -> {
                LanternFishEnum nextEvolution = LanternFishEnum.of(new LanternFish(fishByCount.getKey().lanternFish().internalTimer() - 1));
                add(nextEvolution, fishByCount.getValue());
            }
        }
    }

    public Long count() {
        return fishByCount.values()
                .stream()
                .mapToLong(Long::longValue)
                .sum();
    }
}
