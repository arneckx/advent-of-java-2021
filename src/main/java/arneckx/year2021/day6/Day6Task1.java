package arneckx.year2021.day6;

import arneckx.TaskDeprecated;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day6Task1 extends TaskDeprecated<Long, Collection<LanternFish>> {

    private static final int DAYS = 80;

    public Day6Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Collection<LanternFish>> parser() {
        return LanternFish::map;
    }

    @Override
    public Long execute() {
        School school = new School(input().stream()
                .flatMap(Collection::stream)
                .map(LanternFishEnum::of)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));

        for (int day = 0; day < DAYS; day++) {
            School newSchool = new School();
            for (Map.Entry<LanternFishEnum, Long> fishByCount : school.fish().entrySet()) {
                newSchool.add(fishByCount);
            }
            school = newSchool;
        }

        return school.count();
    }
}
