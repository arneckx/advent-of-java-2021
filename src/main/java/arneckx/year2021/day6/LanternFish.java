package arneckx.year2021.day6;

import java.util.ArrayList;
import java.util.Collection;

public class LanternFish {

    private int internalTimer;

    public LanternFish(int internalTimer) {
        this.internalTimer = internalTimer;
    }

    public static Collection<LanternFish> map(String line) {
        Collection<LanternFish> fish = new ArrayList<>();
        String[] splittedLine = line.split(",");

        for (String timer : splittedLine) {
            fish.add(new LanternFish(Integer.parseInt(timer.trim())));
        }
        return fish;
    }

    public int internalTimer() {
        return internalTimer;
    }

    public void reset() {
        internalTimer = 6;
    }

    public void newDay() {
        internalTimer--;
    }
}
