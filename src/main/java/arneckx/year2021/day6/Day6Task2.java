package arneckx.year2021.day6;

import arneckx.TaskDeprecated;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day6Task2 extends TaskDeprecated<Long, Collection<LanternFish>> {

    private static final int DAYS = 256;

    public Day6Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Collection<LanternFish>> parser() {
        return LanternFish::map;
    }

    @Override
    public Long execute() {
        List<LanternFish> lanternFish = input().stream().flatMap(Collection::stream).collect(Collectors.toList());

        School school = new School(lanternFish.stream()
                .map(LanternFishEnum::of)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));

        for (int day = 0; day < DAYS; day++) {
            School newSchool = new School();
            for (Map.Entry<LanternFishEnum, Long> fishByCount : school.fish().entrySet()) {
                newSchool.add(fishByCount);
            }
            school = newSchool;
        }

        return school.count();
    }
}
