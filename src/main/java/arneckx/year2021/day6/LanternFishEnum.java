package arneckx.year2021.day6;

public enum LanternFishEnum {
    ZERO(new LanternFish(0)),
    ONE(new LanternFish(1)),
    TWO(new LanternFish(2)),
    THREE(new LanternFish(3)),
    FOUR(new LanternFish(4)),
    FIVE(new LanternFish(5)),
    SIX(new LanternFish(6)),
    SEVEN(new LanternFish(7)),
    EIGHT(new LanternFish(8));

    private final LanternFish lanternFish;

    LanternFishEnum(LanternFish lanternFish) {
        this.lanternFish = lanternFish;
    }

    public static LanternFishEnum of(LanternFish fish) {
        for (LanternFishEnum lanternFishEnum : LanternFishEnum.values()) {
            if (lanternFishEnum.lanternFish.internalTimer() == fish.internalTimer()) {
                return lanternFishEnum;
            }
        }
        throw new RuntimeException("no enum found for fish with timer: " + fish.internalTimer());
    }

    public LanternFish lanternFish() {
        return lanternFish;
    }
}
