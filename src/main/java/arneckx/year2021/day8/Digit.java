package arneckx.year2021.day8;

public enum Digit {
    ONE(2),
    SEVEN(3),
    FOUR(4),
    TWO(5),
    THREE(5),
    FIVE(5),
    ZERO(6),
    SIX(6),
    NINE(6),
    EIGHT(7);

    private final int amountOfDigits;

    Digit(int amountOfDigits) {

        this.amountOfDigits = amountOfDigits;
    }

    public int amountOfDigits() {
        return amountOfDigits;
    }
}
