package arneckx.year2021.day8;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day8Task2 extends TaskDeprecated<Integer, FourDigitScreen> {

    public Day8Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, FourDigitScreen> parser() {
        return FourDigitScreen::map;
    }

    @Override
    public Integer execute() {
        List<FourDigitScreen> input = input();
        List<Integer> results = new ArrayList<>();
        int result = 0;

        for (FourDigitScreen fourDigitScreen : input) {
            results.add(fourDigitScreen.decode());
            result += fourDigitScreen.decode();
        }

        return result;
    }
}
