package arneckx.year2021.day8;

import static arneckx.year2021.day8.Digit.EIGHT;
import static arneckx.year2021.day8.Digit.FOUR;
import static arneckx.year2021.day8.Digit.ONE;
import static arneckx.year2021.day8.Digit.SEVEN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class FourDigitScreen {

    private final List<String> uniqueSignalPatterns;
    private final List<String> fourDigits;

    private String zero;
    private String one;
    private String two;
    private String three;
    private String four;
    private String five;
    private String six;
    private String seven;
    private String eight;
    private String nine;

    public FourDigitScreen(List<String> uniqueSignalPatterns, List<String> fourDigits) {
        if (uniqueSignalPatterns.size() > 10) {
            throw new RuntimeException("unique signal patterns are too big");
        }
        if (fourDigits.size() > 4) {
            throw new RuntimeException("four digits size is too big");
        }
        this.uniqueSignalPatterns = uniqueSignalPatterns;
        this.fourDigits = fourDigits;

        calculateOneFourSevenEight();
        calculateOtherDigits();
    }

    private void calculateOtherDigits() {
        while (!allDigitAreFound()) {
            for (String pattern : uniqueSignalPatterns) {
                if (pattern.toCharArray().length == 5) {
                    if (containsAllDigitsOfOne(pattern)) {
                        three = pattern;
                    } else {
                        if (isATwo(pattern)) {
                            two = pattern;
                        } else {
                            five = pattern;
                        }
                    }
                }
                if (pattern.toCharArray().length == 6) {
                    if (!containsAllDigitsOfOne(pattern)) {
                        six = pattern;
                    } else {
                        if (containsAllDigitsOfFour(pattern)) {
                            nine = pattern;
                        } else {
                            zero = pattern;
                        }
                    }
                }
            }
        }
    }

    private boolean allDigitAreFound() {
        return zero() != null &&
                one() != null &&
                two() != null &&
                three() != null &&
                four() != null &&
                five() != null &&
                six() != null &&
                seven() != null &&
                eight() != null &&
                nine() != null;
    }

    private boolean isATwo(String pattern) {
        char characterForTwo = characterForTwo();

        return pattern.indexOf(characterForTwo) != -1;
    }

    public char characterForTwo() {
        if (six() != null && eight() != null) {
            for (char character : eight().toCharArray()) {
                if (six().indexOf(character) == -1) {
                    return character;
                }
            }
        }

        return '@';
    }

    private boolean containsAllDigits(String pattern, String digit) {
        Collection<Boolean> presentDigits = new ArrayList<>();
        for (char character : digit.toCharArray()) {
            if (pattern.indexOf(character) == -1) {
                return false;
            } else {
                presentDigits.add(true);
            }
        }

        if (pattern.toCharArray().length == presentDigits.size()) {
            return true;
        }

        return false;
    }

    private boolean containsAllDigitsOfOne(String pattern) {
        return pattern.indexOf(one().toCharArray()[0]) != -1 &&
                pattern.indexOf(one().toCharArray()[1]) != -1;
    }

    private boolean containsAllDigitsOfFour(String pattern) {
        return pattern.indexOf(four().toCharArray()[0]) != -1 &&
                pattern.indexOf(four().toCharArray()[1]) != -1 &&
                pattern.indexOf(four().toCharArray()[2]) != -1 &&
                pattern.indexOf(four().toCharArray()[3]) != -1;
    }

    private void calculateOneFourSevenEight() {
        for (String pattern : uniqueSignalPatterns) {
            if (pattern.toCharArray().length == ONE.amountOfDigits()) {
                one = pattern;
            }
            if (pattern.toCharArray().length == FOUR.amountOfDigits()) {
                four = pattern;
            }
            if (pattern.toCharArray().length == SEVEN.amountOfDigits()) {
                seven = pattern;
            }
            if (pattern.toCharArray().length == EIGHT.amountOfDigits()) {
                eight = pattern;
            }
        }

        if (one() == null || four() == null || seven() == null || eight() == null) {
            throw new RuntimeException("Failed to find one of the easy digits");
        }
    }

    public static FourDigitScreen map(String line) {
        String[] splittedLine = line.split("\\|");

        String[] uniqueSignalPattern = splittedLine[0].trim().split(" ");
        String[] fourDigitsScreen = splittedLine[1].trim().split(" ");

        return new FourDigitScreen(Arrays.asList(uniqueSignalPattern), Arrays.asList(fourDigitsScreen));
    }

    public int countDigits(List<Integer> amountOfDigit) {
        int counter = 0;
        for (String fourDigit : fourDigits) {
            if (amountOfDigit.contains(fourDigit.toCharArray().length)) {
                counter++;
            }
        }
        return counter;
    }

    public int decode() {
        StringBuilder fourDigitBuilder = new StringBuilder();
        for (String fourDigit : fourDigits) {
            if (containsAllDigits(fourDigit, zero())) {
                fourDigitBuilder.append(0);
            }
            if (containsAllDigits(fourDigit, one())) {
                fourDigitBuilder.append(1);
            }
            if (containsAllDigits(fourDigit, two())) {
                fourDigitBuilder.append(2);
            }
            if (containsAllDigits(fourDigit, three())) {
                fourDigitBuilder.append(3);
            }
            if (containsAllDigits(fourDigit, four())) {
                fourDigitBuilder.append(4);
            }
            if (containsAllDigits(fourDigit, five())) {
                fourDigitBuilder.append(5);
            }
            if (containsAllDigits(fourDigit, six())) {
                fourDigitBuilder.append(6);
            }
            if (containsAllDigits(fourDigit, seven())) {
                fourDigitBuilder.append(7);
            }
            if (containsAllDigits(fourDigit, eight())) {
                fourDigitBuilder.append(8);
            }
            if (containsAllDigits(fourDigit, nine())) {
                fourDigitBuilder.append(9);
            }
        }
        if (fourDigitBuilder.toString().isEmpty()) {
            throw new RuntimeException("Failed to decode");
        }
        return Integer.parseInt(fourDigitBuilder.toString());
    }

    public String zero() {
        return zero;
    }

    public String one() {
        return one;
    }

    public String two() {
        return two;
    }

    public String three() {
        return three;
    }

    public String four() {
        return four;
    }

    public String five() {
        return five;
    }

    public String six() {
        return six;
    }

    public String seven() {
        return seven;
    }

    public String eight() {
        return eight;
    }

    public String nine() {
        return nine;
    }
}
