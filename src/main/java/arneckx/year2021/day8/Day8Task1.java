package arneckx.year2021.day8;

import arneckx.TaskDeprecated;

import java.util.List;
import java.util.function.Function;

public class Day8Task1 extends TaskDeprecated<Integer, FourDigitScreen> {

    public Day8Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, FourDigitScreen> parser() {
        return FourDigitScreen::map;
    }

    @Override
    public Integer execute() {
        List<FourDigitScreen> input = input();
        int instances = 0;

        for (FourDigitScreen fourDigitScreen : input) {
            instances += fourDigitScreen.countDigits(List.of(
                    Digit.ONE.amountOfDigits(),
                    Digit.FOUR.amountOfDigits(),
                    Digit.SEVEN.amountOfDigits(),
                    Digit.EIGHT.amountOfDigits()
            ));
        }

        return instances;
    }
}
