package arneckx.year2021.day14;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day14Task1 extends TaskDeprecated<Long, String> {

    public static final int STEPS = 10;
    private final HashMap<PolymerPair, PolymerTemplate> templates = new HashMap<>();
    private final List<PolymerPair> polymerPairs = new ArrayList<>();

    public Day14Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Long execute() {
        mapInput();

        Map<PolymerPair, Long> calculate = calculate();

        Map<String, Long> countForElement = calculate.entrySet()
                .stream()
                .flatMap(entry -> Stream.of(
                        new CharacterCounting(entry.getKey().firstElement(), entry.getValue())))
                .collect(Collectors.toMap(
                        CharacterCounting::character,
                        CharacterCounting::count,
                        (value1, value2) -> value1 += value2));

        long max = countForElement.values()
                .stream()
                .mapToLong(num -> num)
                .max()
                .orElseThrow();
        long min = countForElement.values()
                .stream()
                .mapToLong(num -> num)
                .min()
                .orElseThrow();

        return max - min;
    }

    private Map<PolymerPair, Long> calculate() {
        List<Map<PolymerPair, Long>> mapsPairCounting = new ArrayList<>();
        for (PolymerPair polymerPair : polymerPairs) {
            if (templates.containsKey(polymerPair)) {
                mapsPairCounting.add(templates.get(polymerPair).calculate(STEPS, templates));
            } else {
                mapsPairCounting.add(Map.of(polymerPair, 1L));
            }
        }

        return mapsPairCounting.stream()
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (value1, value2) -> value1 += value2));
    }

    private void mapInput() {
        List<String> input = input();
        mapPolymerPairs(input.get(0));

        input.remove(0);
        input.remove(0);

        mapTemplates(input);
    }

    private void mapPolymerPairs(String firstLine) {
        char[] polymerChars = firstLine.toCharArray();
        String firstCharacter = null;
        String secondCharacter = null;

        for (char character : polymerChars) {
            if (firstCharacter == null) {
                firstCharacter = String.valueOf(character);
            } else {
                secondCharacter = String.valueOf(character);
            }

            if (secondCharacter != null) {
                polymerPairs.add(new PolymerPair(firstCharacter, secondCharacter));

                firstCharacter = secondCharacter;
                secondCharacter = null;
            }
        }

        if (firstCharacter != null) {
            polymerPairs.add(new PolymerPair(firstCharacter, secondCharacter));
        }
    }

    private void mapTemplates(List<String> input) {
        for (String line : input) {
            String[] split = line.split("->");

            String firstElement = String.valueOf(split[0].trim().toCharArray()[0]);
            String secondElement = String.valueOf(split[0].toCharArray()[1]);
            String elementToBeInserted = split[1].trim();

            PolymerPair pair = new PolymerPair(firstElement, secondElement);
            templates.put(pair, new PolymerTemplate(pair, elementToBeInserted));
        }
    }
}
