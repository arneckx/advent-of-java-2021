package arneckx.year2021.day14;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record PolymerTemplate(PolymerPair pair, String insertionElement) {

    public Map<PolymerPair, Long> calculate(int steps, Map<PolymerPair, PolymerTemplate> templates) {
        Map<PolymerPair, Long> polymerPairCounting = initCountingMap();

        for (int step = 1; step < steps; step++) {
            Map<PolymerPair, Long> newMap = new HashMap<>();
            for (Map.Entry<PolymerPair, Long> polymerPairLongEntry : polymerPairCounting.entrySet()) {
                if (templates.containsKey(polymerPairLongEntry.getKey())) {
                    List<PolymerPair> next = templates.get(polymerPairLongEntry.getKey()).next();
                    for (PolymerPair polymerPair : next) {
                        if (newMap.containsKey(polymerPair)) {
                            Long currentCount = newMap.get(polymerPair);
                            currentCount += polymerPairLongEntry.getValue();
                            newMap.put(polymerPair, currentCount);
                        } else {
                            newMap.put(polymerPair, polymerPairLongEntry.getValue());
                        }
                    }
                }
            }
            polymerPairCounting = newMap;
        }
        return polymerPairCounting;
    }

    private Map<PolymerPair, Long> initCountingMap() {
        Map<PolymerPair, Long> polymerPairCounting = new HashMap<>();

        for (PolymerPair polymerPair : next()) {
            if (polymerPairCounting.containsKey(polymerPair)) {
                Long currentCount = polymerPairCounting.get(polymerPair);
                polymerPairCounting.put(polymerPair, currentCount++);
            } else {
                polymerPairCounting.put(polymerPair, 1L);
            }
        }
        return polymerPairCounting;
    }

    private List<PolymerPair> next() {
        return List.of(
                new PolymerPair(pair.firstElement(), insertionElement),
                new PolymerPair(insertionElement, pair.secondElement()
                ));
    }
}
