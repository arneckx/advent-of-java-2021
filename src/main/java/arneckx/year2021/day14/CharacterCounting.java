package arneckx.year2021.day14;

public class CharacterCounting {
    private final String character;
    private final Long count;

    public CharacterCounting(String character, Long count) {
        this.character = character;
        this.count = count;
    }

    public String character() {
        return character;
    }

    public Long count() {
        return count;
    }
}
