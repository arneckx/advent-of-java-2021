package arneckx.year2021.day14;

public record PolymerPair(String firstElement, String secondElement) {
}
