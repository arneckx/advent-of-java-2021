package arneckx.year2021.day5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class Vent {
    private final Coordinate start;
    private final Coordinate end;
    private final List<Coordinate> coveredPoints = new ArrayList<>();
    private final boolean includeDiagonal;

    public Vent(boolean includeDiagonal, Coordinate start, Coordinate end) {
        this.start = start;
        this.end = end;
        this.includeDiagonal = includeDiagonal;

        calculateCoveredPoints();
    }

    private void calculateCoveredPoints() {
        if (isVertical()) {
            calculateVerticalCoveredPoints();
            return;
        }
        if (isHorizontal()) {
            calculateHorizontalCoveredPoints();
            return;
        }
        if (is45Diagonal()) {
            calculateDiagonal();
            return;
        }
    }

    private void calculateDiagonal() {
        //        0,0 -> 8,8

        List<Integer> rows = new ArrayList<>();
        List<Integer> columns = new ArrayList<>();
        for (int i = 0; i <= Math.abs(start.row() - end.row()); i++) {
            if (start.row() > end.row()) {
                rows.add((int) (start.row() - i));
            }
            if (end().row() > start.row()) {
                rows.add((int) (start.row() + i));
            }
        }

        for (int i = 0; i <= Math.abs(start.column() - end.column()); i++) {
            if (start.column() > end.column()) {
                columns.add((int) (start.column() - i));
            }
            if (end().column() > start.column()) {
                columns.add((int) (start.column() + i));
            }
        }

        for (int counter = 0; counter <= Math.abs(start.row() - end.row()); counter++) {
            coveredPoints.add(new Coordinate(columns.get(counter), rows.get(counter)));
        }
    }

    private void calculateVerticalCoveredPoints() {
        if (start.row() > end.row()) {
            coveredPoints.add(new Coordinate(start.column(), start.row()));
            for (long counter = 1; counter < (start.row() - end.row()); counter++) {
                coveredPoints.add(new Coordinate(start.column(), start.row() - counter));
            }
            coveredPoints.add(new Coordinate(start.column(), end.row()));
        }
        if (start.row() < end.row()) {
            coveredPoints.add(new Coordinate(start.column(), end.row()));
            for (long counter = 1; counter < (end.row() - start.row()); counter++) {
                coveredPoints.add(new Coordinate(start.column(), end.row() - counter));
            }
            coveredPoints.add(new Coordinate(start.column(), start.row()));
        }
    }

    private void calculateHorizontalCoveredPoints() {
        if (start.column() > end.column()) {
            coveredPoints.add(new Coordinate(start.column(), start.row()));
            for (long counter = 1; counter < (start.column() - end.column()); counter++) {
                coveredPoints.add(new Coordinate(start.column() - counter, start.row()));
            }
            coveredPoints.add(new Coordinate(end.column(), start.row()));
        }
        if (start.column() < end.column()) {
            coveredPoints.add(new Coordinate(end.column(), start.row()));
            for (long counter = 1; counter < (end.column() - start.column()); counter++) {
                coveredPoints.add(new Coordinate(end.column() - counter, start.row()));
            }
            coveredPoints.add(new Coordinate(start.column(), start.row()));
        }
    }

    private boolean is45Diagonal() {
        if (!includeDiagonal) {
            return false;
        }
        return Math.abs(start.column() - end.column()) == Math.abs(start.row() - end.row());
    }

    private boolean isVertical() {
        return start.column() == end.column();
    }

    private boolean isHorizontal() {
        return start.row() == end.row();
    }

    public static Vent map(String line, boolean includeDiagonal) {
        String workingString = line.replace("->", ",").replace(" ", "");
        String[] splittedString = workingString.split(",");
        if (Arrays.stream(splittedString).toList().size() != 4) {
            throw new RuntimeException("unexpected size for line found");
        }

        return new Vent(
                includeDiagonal,
                new Coordinate(Long.parseLong(splittedString[0]), Long.parseLong(splittedString[1])),
                new Coordinate(Long.parseLong(splittedString[2]), Long.parseLong(splittedString[3]))
        );
    }

    public List<Coordinate> coveredPoints() {
        return coveredPoints;
    }

    public Coordinate start() {
        return start;
    }

    public Coordinate end() {
        return end;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (Vent) obj;
        return Objects.equals(this.start, that.start) &&
                Objects.equals(this.end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "Vent[" +
                "start=" + start + ", " +
                "end=" + end + ']';
    }
}
