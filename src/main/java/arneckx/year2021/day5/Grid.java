package arneckx.year2021.day5;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public record Grid(List<Vent> vents) {

    public Set<Coordinate> dangerousZones() {
        return vents.stream()
                .flatMap(vent -> vent.coveredPoints().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(counting -> counting.getValue() >= 2)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        StringBuilder grid = new StringBuilder();
        Map<Coordinate, Long> coveredVents = vents.stream()
                .flatMap(vent -> vent.coveredPoints().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for (int row = 0; row <= maxRow(); row++) {
            for (int column = 0; column <= maxColumn(); column++) {
                if (coveredVents.containsKey(new Coordinate(column, row))) {
                    if (coveredVents.get(new Coordinate(column, row)) >= 2) {
                        grid.append("# ");
                    } else {
                        grid.append("x ");
                    }
                } else {
                    grid.append(". ");
                }
            }
            grid.append("\n");
        }

        return grid.toString();
    }

    private long maxColumn() {
        return vents.stream()
                .map(vent -> List.of(vent.start().column(), vent.end().column()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .stream()
                .mapToLong(number -> number)
                .max()
                .orElseThrow();
    }

    private long maxRow() {
        return vents.stream()
                .map(vent -> List.of(vent.start().row(), vent.end().row()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .stream()
                .mapToLong(number -> number)
                .max()
                .orElseThrow();
    }
}
