package arneckx.year2021.day5;

import arneckx.TaskDeprecated;

import java.util.function.Function;

public class Day5Task1 extends TaskDeprecated<Integer, Vent> {

    private static final boolean INCLUDE_DIAGONAL = false;

    public Day5Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Vent> parser() {
        return line -> Vent.map(line, INCLUDE_DIAGONAL);
    }

    @Override
    public Integer execute() {
        Grid grid = new Grid(input());

        return grid.dangerousZones().size();
    }
}
