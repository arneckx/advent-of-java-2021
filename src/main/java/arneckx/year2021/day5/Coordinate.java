package arneckx.year2021.day5;

public record Coordinate(long column, long row) {
}
