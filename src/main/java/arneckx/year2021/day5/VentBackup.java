package arneckx.year2021.day5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class VentBackup {
    private final Coordinate start;
    private final Coordinate end;
    private final List<Coordinate> coveredPoints = new ArrayList<>();
    private final boolean includeDiagonal;

    public VentBackup(boolean includeDiagonal, Coordinate start, Coordinate end) {
        this.start = start;
        this.end = end;
        this.includeDiagonal = includeDiagonal;

        calculateCoveredPoints();
    }

    private void calculateCoveredPoints() {
        if (is45Diagonal()) {
            List<Long> possibleColumnPositions = possibleColumnPositions();
            List<Long> possibleRowPositions = possibleRowPositions();
            for (int index = 0; index < possibleRowPositions.size(); index++) {
                coveredPoints.add(new Coordinate(possibleColumnPositions.get(index), possibleRowPositions.get(index)));
            }
        } else {
            if (isHorizontal() || isVertical()) {
                for (Long row : possibleRowPositions()) {
                    for (Long column : possibleColumnPositions()) {
                        coveredPoints.add(new Coordinate(column, row));
                    }
                }
            }
        }
    }

    private boolean is45Diagonal() {
        if (!includeDiagonal || isHorizontal() || isVertical()) {
            return false;
        }

        return differenceBetweenRows() == differenceBetweenColumns();
    }

    public long differenceBetweenRows() {
        long greatestRow;
        long lowestRow;
        if (start.row() > end.row()) {
            greatestRow = start.row();
            lowestRow = end.row();
        } else {
            greatestRow = end.row();
            lowestRow = start.row();
        }
        return greatestRow - lowestRow;
    }

    public long differenceBetweenColumns() {
        long greatestColumn;
        long lowestColumn;
        if (start.column() > end.column()) {
            greatestColumn = start.column();
            lowestColumn = end.column();
        } else {
            greatestColumn = end.column();
            lowestColumn = start.column();
        }
        return greatestColumn - lowestColumn;
    }

    private boolean isVertical() {
        return start.column() == end.column();
    }

    private boolean isHorizontal() {
        return start.row() == end.row();
    }

    private List<Long> possibleRowPositions() {
        Set<Long> possibleRowPositions = new HashSet<>();
        possibleRowPositions.add(start().row());
        possibleRowPositions.add(end().row());
        if (start.row() > end.row()) {
            for (int i = 0; i < differenceBetweenRows(); i++) {
                possibleRowPositions.add(start().row() - differenceBetweenRows() + i);
            }
            return reversed(possibleRowPositions);
        }
        if (start.row() < end.row()) {
            for (int i = 0; i < differenceBetweenRows(); i++) {
                possibleRowPositions.add(end.row() - differenceBetweenRows() + i);
            }
        }
        return new ArrayList<>(possibleRowPositions);
    }

    private List<Long> possibleColumnPositions() {
        Set<Long> possibleColumnPositions = new HashSet<>();
        possibleColumnPositions.add(start().column());
        possibleColumnPositions.add(end().column());
        if (start.column() > end.column()) {
            for (int i = 0; i < differenceBetweenColumns(); i++) {
                possibleColumnPositions.add(start.column() - differenceBetweenColumns() + i);
            }
            return reversed(possibleColumnPositions);
        }
        if (start.column() < end.column()) {
            for (int i = 0; i < differenceBetweenColumns(); i++) {
                possibleColumnPositions.add(end.column() - differenceBetweenColumns() + i);
            }
        }
        return new ArrayList<>(possibleColumnPositions);
    }

    private List<Long> reversed(Set<Long> integers) {
        ArrayList<Long> list = new ArrayList<>(integers);
        Collections.reverse(list);
        return list;
    }

    public static VentBackup map(String line, boolean includeDiagonal) {
        String workingString = line.replace("->", ",").replace(" ", "");
        String[] splittedString = workingString.split(",");
        if (Arrays.stream(splittedString).toList().size() != 4) {
            throw new RuntimeException("unexpected size for line found");
        }

        return new VentBackup(
                includeDiagonal,
                new Coordinate(Long.parseLong(splittedString[0]), Long.parseLong(splittedString[1])),
                new Coordinate(Long.parseLong(splittedString[2]), Long.parseLong(splittedString[3]))
        );
    }

    public List<Coordinate> coveredPoints() {
        return coveredPoints;
    }

    public Coordinate start() {
        return start;
    }

    public Coordinate end() {
        return end;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (VentBackup) obj;
        return Objects.equals(this.start, that.start) &&
                Objects.equals(this.end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "Vent[" +
                "start=" + start + ", " +
                "end=" + end + ']';
    }
}
