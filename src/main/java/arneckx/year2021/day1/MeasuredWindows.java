package arneckx.year2021.day1;

import java.util.ArrayList;
import java.util.List;

class MeasuredWindows {

    private static final int WINDOW_SIZE = 3;

    private final List<Integer> allWindows = new ArrayList<>();
    private int nextWindow;

    MeasuredWindows() {
        allWindows.add(0);
        nextWindow = 1;
    }

    List<Integer> windows() {
        List<Integer> nextWindows;
        if (allWindows.size() < WINDOW_SIZE) {
            nextWindows = new ArrayList<>(allWindows);
        } else {
            nextWindows = new ArrayList<>(allWindows.subList(allWindows.size() - WINDOW_SIZE, allWindows.size()));
        }
        allWindows.add(nextWindow);
        nextWindow++;
        return nextWindows;
    }
}
