package arneckx.year2021.day1;

import arneckx.TaskDeprecated;
import static java.util.Objects.nonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Day1Task2 extends TaskDeprecated<Integer, Integer> {

    private Integer previousDepth;
    private int amountOfIncreasedSweeps;
    private final Map<Integer, Integer> depthPerWindow = new HashMap<>();

    public Day1Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Integer> parser() {
        return Integer::valueOf;
    }

    @Override
    public Integer execute() {
        MeasuredWindows threeMeasuredWindow = new MeasuredWindows();

        for (Integer sweep : input()) {
            List<Integer> windows = threeMeasuredWindow.windows();
            for (Integer window : windows) {
                if (depthPerWindow.containsKey(window)) {
                    Integer currentWindowDepth = depthPerWindow.get(window);
                    depthPerWindow.put(window, (currentWindowDepth + sweep));
                } else {
                    depthPerWindow.put(window, sweep);
                }
            }
        }

        for (Map.Entry<Integer, Integer> windowDepth : depthPerWindow.entrySet()) {
            System.out.printf("window %s with depth %s%n", windowDepth.getKey(), windowDepth.getValue());

            if (nonNull(previousDepth) && hasIncreased(previousDepth, windowDepth.getValue())) {
                amountOfIncreasedSweeps++;
                System.out.println("Increasing");
            }

            previousDepth = windowDepth.getValue();
        }

        return amountOfIncreasedSweeps;
    }

    private static boolean hasIncreased(Integer previousSweep, Integer sweep) {
        return sweep > previousSweep;
    }
}
