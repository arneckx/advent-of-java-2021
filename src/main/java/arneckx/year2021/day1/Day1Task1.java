package arneckx.year2021.day1;

import arneckx.TaskDeprecated;
import static java.util.Objects.nonNull;

import java.util.function.Function;

public class Day1Task1 extends TaskDeprecated<Integer, Integer> {

    private Integer previousSweep;
    private int amountOfIncreasedSweeps;

    public Day1Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Integer> parser() {
        return Integer::valueOf;
    }

    @Override
    public Integer execute() {
        for (Integer sweep : input()) {

            if (nonNull(previousSweep) && hasIncreased(previousSweep, sweep)) {
                amountOfIncreasedSweeps++;
            }

            previousSweep = sweep;
        }

        return amountOfIncreasedSweeps;
    }

    private static boolean hasIncreased(Integer previousSweep, Integer sweep) {
        return sweep > previousSweep;
    }
}
