package arneckx.year2021.day17;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day17Task2 extends TaskDeprecated<Long, TargetArea> {

    public Day17Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, TargetArea> parser() {
        return TargetArea::map;
    }

    @Override
    public Long execute() {
        TargetArea targetArea = input().get(0);

        List<Probe> probes = new ArrayList<>();
        for (int x = -200; x < 200; x++) {
            for (int y = -117; y < 200; y++) {
                probes.add(Probe.launch(x, y, targetArea));
            }
        }

        return probes.stream()
                .filter(Probe::hasCrossedTarget)
                .map(Probe::initialVelocitySettings)
                .count();
    }
}
