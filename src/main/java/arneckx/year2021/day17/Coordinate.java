package arneckx.year2021.day17;

public record Coordinate(int x, int y) {
}
