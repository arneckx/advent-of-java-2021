package arneckx.year2021.day17;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day17Task1 extends TaskDeprecated<Integer, TargetArea> {

    public Day17Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, TargetArea> parser() {
        return TargetArea::map;
    }

    @Override
    public Integer execute() {
        TargetArea targetArea = input().get(0);

        List<Probe> probes = new ArrayList<>();
        for (int x = -200; x < 200; x++) {
            for (int y = -117; y < 200; y++) {
                probes.add(Probe.launch(x, y, targetArea));
            }
        }

        return probes.stream()
                .filter(Probe::hasCrossedTarget)
                .mapToInt(Probe::maximumYPosition)
                .max()
                .orElseThrow();
    }
}
