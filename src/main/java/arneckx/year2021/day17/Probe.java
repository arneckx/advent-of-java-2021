package arneckx.year2021.day17;

public class Probe {
    private Coordinate position = new Coordinate(0, 0);
    private final Coordinate initialVelocitySettings;
    private int xVelocity;
    private int yVelocity;
    private int maximumYPosition = 0;
    private boolean hasCrossedTarget = false;

    private Probe(int xVelocity, int yVelocity) {
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
        initialVelocitySettings = new Coordinate(xVelocity, yVelocity);
    }

    public void next() {
        position = new Coordinate(position.x() + xVelocity, position.y() + yVelocity);

        if (xVelocity > 0) {
            xVelocity--;
        }
        if (xVelocity < 0) {
            xVelocity++;
        }

        yVelocity--;

        if (position.y() > maximumYPosition) {
            maximumYPosition = position.y();
        }
    }

    public static Probe launch(int xVelocity, int yVelocity, TargetArea targetArea) {
        Probe probe = new Probe(xVelocity, yVelocity);

        while (!probe.isInside(targetArea) && !probe.hasMissed(targetArea)) {
            probe.next();
        }

        return probe;
    }

    private boolean hasMissed(TargetArea targetArea) {
        return position.y() < targetArea.lowestPoint().y();
    }

    public boolean isInside(TargetArea targetArea) {
        hasCrossedTarget = targetArea.positions().contains(position);
        return hasCrossedTarget;
    }

    public Coordinate position() {
        return position;
    }

    public int maximumYPosition() {
        return maximumYPosition;
    }

    public boolean hasCrossedTarget() {
        return hasCrossedTarget;
    }

    public Coordinate initialVelocitySettings() {
        return initialVelocitySettings;
    }
}
