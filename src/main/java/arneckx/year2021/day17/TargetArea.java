package arneckx.year2021.day17;

import java.util.ArrayList;
import java.util.List;

public class TargetArea {
    private final List<Coordinate> positions = new ArrayList<>();

    private TargetArea() {
    }

    public List<Coordinate> positions() {
        return positions;
    }

    public static TargetArea map(String line) {
        String cleanLine = line.replace("target area: ", "");
        String[] split = cleanLine.split(", ");

        String[] xTarget = split[0].replace("x=", "").split("\\.\\.");
        String[] yTarget = split[1].replace("y=", "").split("\\.\\.");

        TargetArea targetArea = new TargetArea();

        for (int x = Integer.parseInt(xTarget[0]); x <= Integer.parseInt(xTarget[1]); x++) {
            for (int y = Integer.parseInt(yTarget[0]); y <= Integer.parseInt(yTarget[1]); y++) {
                targetArea.positions.add(new Coordinate(x, y));
            }
        }

        return targetArea;
    }

    public Coordinate lowestPoint() {
        return positions.get(0);
    }
}
