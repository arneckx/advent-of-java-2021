package arneckx.year2021.day9;

import java.util.List;

public record Coordinate(int row, int column) {

    public List<Coordinate> around() {
        return List.of(
                new Coordinate(row - 1, column),
                new Coordinate(row + 1, column),
                new Coordinate(row, column - 1),
                new Coordinate(row, column + 1)
        );
    }
}
