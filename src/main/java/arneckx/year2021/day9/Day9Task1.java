package arneckx.year2021.day9;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Day9Task1 extends TaskDeprecated<Integer, String> {

    private final HashMap<Coordinate, Integer> numbersForCoordinate = new HashMap<>();
    private final List<Integer> riskPoints = new ArrayList<>();

    public Day9Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapToNumbersPerCoordinate();

        for (Map.Entry<Coordinate, Integer> numberForCoordinate : numbersForCoordinate.entrySet()) {
            Coordinate coordinate = numberForCoordinate.getKey();
            List<Integer> higherNumbersAround = new ArrayList<>();
            int numberOfCoordinatesAround = 0;
            List<Coordinate> coordinatesAround = coordinate.around();
            for (Coordinate nextTo : coordinatesAround) {
                if (numbersForCoordinate.containsKey(nextTo)) {
                    numberOfCoordinatesAround++;
                    Integer numberNextTo = numbersForCoordinate.get(nextTo);
                    if (numberNextTo > numberForCoordinate.getValue()) {
                        higherNumbersAround.add(numberNextTo);
                    }
                }
            }

            if (higherNumbersAround.size() == numberOfCoordinatesAround) {
                // Risky point
                riskPoints.add(numberForCoordinate.getValue());
            }
        }

        return riskPoints.stream()
                .mapToInt(number -> number + 1)
                .sum();
    }

    private void mapToNumbersPerCoordinate() {
        List<String> input = input();
        for (int row = 0; row < input.size(); row++) {
            char[] rowNumbers = input.get(row).toCharArray();
            for (int column = 0; column < rowNumbers.length; column++) {
                int number = Integer.parseInt(String.valueOf(rowNumbers[column]));
                numbersForCoordinate.put(new Coordinate(row, column), number);
            }
        }
    }
}
