package arneckx.year2021.day9;

import java.util.HashSet;
import java.util.Set;

public class Basin {
    private final Set<Coordinate> points = new HashSet<>();

    public void addPointToBasin(Coordinate coordinate) {
        points.add(coordinate);
    }

    public boolean contains(Coordinate coordinate) {
        return points.stream()
                .anyMatch(coordinateInBasin -> coordinateInBasin.equals(coordinate));
    }

    public int size() {
        return points.size();
    }

    public Set<Coordinate> points() {
        return points;
    }
}
