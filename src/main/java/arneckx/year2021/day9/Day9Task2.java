package arneckx.year2021.day9;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day9Task2 extends TaskDeprecated<Integer, String> {

    private final HashMap<Coordinate, Integer> numbersForCoordinate = new HashMap<>();
    private final List<Basin> basins = new ArrayList<>();

    public Day9Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer execute() {
        mapToNumbersPerCoordinate();

        for (Map.Entry<Coordinate, Integer> numberForCoordinate : numbersForCoordinate.entrySet()) {
            Coordinate coordinate = numberForCoordinate.getKey();

            if (numberForCoordinate.getValue() == 9 || isAlreadyPresentInBasin(numberForCoordinate)) {
                System.out.println("skip");
            } else {
                createBasin(coordinate);
            }
        }

        List<Basin> orderedBasins = basins.stream()
                .sorted(Comparator.comparing(Basin::size).reversed())
                .collect(Collectors.toList());
        int basin1Size = orderedBasins.get(0).size();
        int basin2Size = orderedBasins.get(1).size();
        int basin3Size = orderedBasins.get(2).size();
        return basin1Size * basin2Size * basin3Size;
    }

    private void createBasin(Coordinate coordinate) {
        Basin basin = new Basin();
        basin.addPointToBasin(coordinate);

        List<Coordinate> checkNextCoordinates = coordinate.around();

        while (checkNextCoordinates.size() != 0) {
            checkNextCoordinates = addCoordinatesAroundBasin(checkNextCoordinates, basin);
        }
        basins.add(basin);
    }

    private List<Coordinate> addCoordinatesAroundBasin(List<Coordinate> coordinates, Basin basin) {
        List<Coordinate> checkNextCoordinates = new ArrayList<>();
        for (Coordinate next : coordinates) {
            if (isPresentInGrid(next)) {
                if (numbersForCoordinate.get(next) != 9) {
                    basin.addPointToBasin(next);
                    checkNextCoordinates.addAll(next.around());
                }
            }
        }
        checkNextCoordinates.removeAll(basin.points());
        return checkNextCoordinates;
    }

    private boolean isPresentInGrid(Coordinate next) {
        return numbersForCoordinate.containsKey(next);
    }

    private boolean isAlreadyPresentInBasin(Map.Entry<Coordinate, Integer> numberForCoordinate) {
        for (Basin basin : basins) {
            if (basin.contains(numberForCoordinate.getKey())) {
                return true;
            }
        }
        return false;
    }

    private void mapToNumbersPerCoordinate() {
        List<String> input = input();
        for (int row = 0; row < input.size(); row++) {
            char[] rowNumbers = input.get(row).toCharArray();
            for (int column = 0; column < rowNumbers.length; column++) {
                int number = Integer.parseInt(String.valueOf(rowNumbers[column]));
                numbersForCoordinate.put(new Coordinate(row, column), number);
            }
        }
    }
}
