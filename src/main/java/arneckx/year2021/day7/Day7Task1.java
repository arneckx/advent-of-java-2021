package arneckx.year2021.day7;

import arneckx.TaskDeprecated;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day7Task1 extends TaskDeprecated<Integer, List<Integer>> {

    private int fuel = 0;

    public Day7Task1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, List<Integer>> parser() {
        return line -> Arrays.stream(line.split(","))
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Integer execute() {
        List<Integer> numbers = input()
                .stream()
                .flatMap(Collection::stream)
                .sorted()
                .collect(Collectors.toList());

        Integer median = numbers.get(numbers.size() / 2);

        for (Integer number : numbers) {
            if (number < median) {
                fuel += median - number;
            } else {
                fuel += number - median;
            }
        }

        return fuel;
    }
}
