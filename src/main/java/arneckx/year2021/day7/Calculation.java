package arneckx.year2021.day7;

import java.util.ArrayList;
import java.util.List;

public class Calculation {

    private int fuel = 0;
    private int fuelStep = 1;

    private final List<Integer> numbers;

    public Calculation(List<Integer> numbers) {
        this.numbers = new ArrayList<>(numbers);
    }

    public int calculateFor(int average) {
        fuel = 0;
        fuelStep = 1;

        for (Integer number : numbers) {
            fuelStep = 1;
            if (number < average) {
                int difference = average - number;
                for (int i = 0; i < difference; i++) {
                    fuel += fuelStep;
                    fuelStep++;
                }
            } else {
                int difference = number - average;
                for (int i = 0; i < difference; i++) {
                    fuel += fuelStep;
                    fuelStep++;
                }
            }
        }

        System.out.println("Fuel consumption: " + fuel);
        return fuel;
    }
}
