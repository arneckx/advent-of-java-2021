package arneckx.year2021.day7;

import arneckx.TaskDeprecated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day7Task2 extends TaskDeprecated<Integer, List<Integer>> {

    private int fuel = 0;
    private int fuelStep = 1;
    private int fuelForNumber = 0;

    public Day7Task2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, List<Integer>> parser() {
        return line -> Arrays.stream(line.split(","))
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Integer execute() {
        List<Integer> numbers = input()
                .stream()
                .flatMap(Collection::stream)
                .sorted()
                .collect(Collectors.toList());

        int sumOfNumbers = numbers.stream().mapToInt(number -> number).sum();
        Integer average = Math.toIntExact(Math.round(sumOfNumbers / (double) numbers.size()));

        List<Integer> results = new ArrayList<>();
        Calculation calculation = new Calculation(numbers);

        int deviation = 4;
        for (int i = 0; i < (average - deviation); i++) {
            results.add(calculation.calculateFor(average - i));
        }

        return results.stream().mapToInt(number -> number).min().orElseThrow(() -> new RuntimeException("no result"));
    }
}
