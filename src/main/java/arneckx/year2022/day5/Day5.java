package arneckx.year2022.day5;

import arneckx.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day5 extends Task<String, String> {

    private final List<String> input = input();
    private final Map<Integer, List<Crate>> cratesForStack = new HashMap<>();
    private StringBuilder sb = new StringBuilder();

    public Day5(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return a -> a;
    }

    @Override
    public String task1() {
        cratesPerStack();

        return cratesForStack.values()
                .stream()
                .map(crateStack -> lastCrateOf(crateStack).character())
                .collect(Collectors.joining());
    }

    private void cratesPerStack() {
        boolean blankLineOccurred = false;
        for (String line : input) {
            int stackNumber = 1;
            if (blankLineOccurred) {
                move(line);
            } else {
                for (int i = 0; i < line.length(); i++) {
                    if (sb.length() == 3) {
                        addCrate(stackNumber);
                        stackNumber++;
                    } else {
                        sb.append(line.charAt(i));
                    }
                }
                addCrate(stackNumber);
            }
            if (line.isBlank()) {
                blankLineOccurred = true;
                for (Map.Entry<Integer, List<Crate>> integerListEntry : cratesForStack.entrySet()) {
                    Collections.reverse(integerListEntry.getValue());
                }
            }
        }
    }

    private void addCrate(int counter) {
        if (!sb.toString().trim().isBlank() && !isNumber(sb.toString())) {
            cratesForStack.merge(
                    counter,
                    new ArrayList<>(List.of(new Crate(sb.substring(1, 2)))),
                    (a, b) -> {
                        ArrayList<Crate> newCrates = new ArrayList<>(a);
                        newCrates.addAll(b);
                        return newCrates;
                    }
            );
        }
        sb = new StringBuilder();
    }

    private boolean isNumber(String toString) {
        try {
            Integer.valueOf(toString.trim());
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    private void move(String line) {
        // move 1 from 2 to 1
        String[] splitted = line.split("from");
        String[] splitted1 = splitted[1].split("to");
        Integer amountOfCrates = Integer.valueOf(splitted[0].trim().replace("move ", ""));
        Integer from = Integer.valueOf(splitted1[0].trim());
        Integer to = Integer.valueOf(splitted1[1].trim());
        move(amountOfCrates, from, to);
    }

    private void move(Integer amountOfCrates, Integer from, Integer to) {
        List<Crate> cratesFrom = cratesForStack.get(from);
        List<Crate> cratesTo = cratesForStack.get(to);

        for (int i = 0; i < amountOfCrates; i++) {
            Crate crate = lastCrateOf(cratesFrom);
            cratesTo.add(crate);
            cratesFrom.remove(crate);
        }
    }

    private Crate lastCrateOf(List<Crate> stack) {
        return stack.get(stack.size() - 1);
    }

    @Override
    public String task2() {
        return "0";
    }
}
