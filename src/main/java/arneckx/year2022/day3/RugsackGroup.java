package arneckx.year2022.day3;

import static arneckx.year2022.day3.Rugsack.ALPHABET;

import java.util.List;

public class RugsackGroup {

    private final String character;

    public RugsackGroup(Rugsack rugsack1, Rugsack rugsack2, Rugsack rugsack3) {
        for (String character1 : rugsack1.allCharacters()) {
            for (String character2 : rugsack2.allCharacters()) {
                for (String character3 : rugsack3.allCharacters()) {
                    if (character1.equals(character2) && character2.equals(character3)) {
                        this.character = character2;
                        return;
                    }
                }
            }
        }

        throw new IllegalStateException();
    }

    public static RugsackGroup create(List<Rugsack> rugsacks) {
        if (rugsacks.size() == 3){
            return new RugsackGroup(rugsacks.get(0), rugsacks.get(1), rugsacks.get(2));
        }

        throw new IllegalStateException();
    }

    int characterScore() {
        return ALPHABET.indexOf(character) + 1;
    }
}
