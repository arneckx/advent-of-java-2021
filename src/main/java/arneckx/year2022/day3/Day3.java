package arneckx.year2022.day3;

import arneckx.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day3 extends Task<Integer, Rugsack> {

    private List<Rugsack> rugsacks = input();

    public Day3(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, Rugsack> parser() {
        return Rugsack::transform;
    }

    @Override
    public Integer task1() {
        return rugsacks.stream().mapToInt(Rugsack::characterScore).sum();
    }

    @Override
    public Integer task2() {
        List<RugsackGroup> rugsacksGroups = new ArrayList<>();
        List<Rugsack> rugsacks = new ArrayList<>();
        int counter = 0;
        for (Rugsack rugsack : this.rugsacks) {
            counter++;
            rugsacks.add(rugsack);
            if (counter == 3) {
                counter = 0;
                rugsacksGroups.add(RugsackGroup.create(rugsacks));
                rugsacks = new ArrayList<>();
            }
        }

        return rugsacksGroups.stream().mapToInt(RugsackGroup::characterScore).sum();
    }
}
