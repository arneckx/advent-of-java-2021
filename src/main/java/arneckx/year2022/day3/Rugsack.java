package arneckx.year2022.day3;

import java.util.List;

public class Rugsack {

    public static final List<String> ALPHABET = List.of(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    );

    private final String character;
    private final List<String> allCharacters;

    private Rugsack(String character, String allCharacters) {
        this.character = character;
        this.allCharacters = List.of(allCharacters.split(""));
    }

    public static Rugsack transform(String line) {
        String compartment1 = line.substring(0, line.length() / 2);
        String compartment2 = line.substring(line.length() / 2);

        for (String character1 : compartment1.split("")) {
            for (String character2 : compartment2.split("")) {
                if (character1.equals(character2)) {
                    return new Rugsack(character2, line);
                }
            }
        }

        throw new IllegalStateException();
    }

    int characterScore() {
        return ALPHABET.indexOf(character) + 1;
    }

    public List<String> allCharacters() {
        return allCharacters;
    }
}
