package arneckx.year2022.day2;

public class Player1Move {

    private final RockPaperScissor rockPaperScissor;

    public Player1Move(String character) {
        rockPaperScissor = switch (character) {
            case "A" -> RockPaperScissor.ROCK;
            case "B" -> RockPaperScissor.PAPER;
            case "C" -> RockPaperScissor.SCISSOR;
            default -> throw new IllegalStateException();
        };
    }

    public RockPaperScissor rockPaperScissor() {
        return rockPaperScissor;
    }
}
