package arneckx.year2022.day2;

import arneckx.Task;

import java.util.List;
import java.util.function.Function;

public class Day2 extends Task<Integer, RockPaperScissorGame> {

    public Day2(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, RockPaperScissorGame> parser() {
        return RockPaperScissorGame::transformTask1;
    }

    @Override
    protected Function<String, RockPaperScissorGame> parserTask2() {
        return RockPaperScissorGame::transformTask2;
    }

    @Override
    public Integer task1() {
        final List<RockPaperScissorGame> input = input();
        return input.stream().mapToInt(RockPaperScissorGame::score).sum();
    }

    @Override
    public Integer task2() {
        final List<RockPaperScissorGame> input = inputTask2();
        return input.stream().mapToInt(RockPaperScissorGame::score).sum();
    }
}
