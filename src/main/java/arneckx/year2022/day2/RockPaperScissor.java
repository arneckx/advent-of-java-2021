package arneckx.year2022.day2;

public enum RockPaperScissor {
    ROCK(1),
    PAPER(2),
    SCISSOR(3);

    final int score;
    private static final int WINNING = 6;
    private static final int DRAW = 3;
    private static final int LOSING = 0;

    RockPaperScissor(int score) {
        this.score = score;
    }

    int play(RockPaperScissor other) {
        return switch (this) {
            case ROCK -> switch (other) {
                case ROCK -> this.score + DRAW;
                case PAPER -> this.score + LOSING;
                case SCISSOR -> this.score + WINNING;
            };
            case PAPER -> switch (other) {
                case ROCK -> this.score + WINNING;
                case PAPER -> this.score + DRAW;
                case SCISSOR -> this.score + LOSING;
            };
            case SCISSOR -> switch (other) {
                case ROCK -> this.score + LOSING;
                case PAPER -> this.score + WINNING;
                case SCISSOR -> this.score + DRAW;
            };
        };
    }

    RockPaperScissor winningMoveAgainst() {
        return switch (this) {
            case ROCK -> PAPER;
            case PAPER -> SCISSOR;
            case SCISSOR -> ROCK;
        };
    }

    RockPaperScissor losingMoveAgainst() {
        return switch (this) {
            case ROCK -> SCISSOR;
            case PAPER -> ROCK;
            case SCISSOR -> PAPER;
        };
    }
}


