package arneckx.year2022.day2;

public class Player2Move {

    private final RockPaperScissor rockPaperScissor;

    public Player2Move(RockPaperScissor rockPaperScissor) {
        this.rockPaperScissor = rockPaperScissor;
    }

    public Player2Move(String character) {
        rockPaperScissor = switch (character) {
            case "X" -> RockPaperScissor.ROCK;
            case "Y" -> RockPaperScissor.PAPER;
            case "Z" -> RockPaperScissor.SCISSOR;
            default -> throw new IllegalStateException();
        };
    }

    public RockPaperScissor rockPaperScissor() {
        return rockPaperScissor;
    }
}
