package arneckx.year2022.day2;

public class RockPaperScissorGame {

    private final Player1Move opponentMove;
    private final Player2Move yourMove;

    public RockPaperScissorGame(Player1Move opponentMove, Player2Move yourMove) {
        this.opponentMove = opponentMove;
        this.yourMove = yourMove;
    }

    static RockPaperScissorGame transformTask1(String line) {
        String[] splitted = line.split(" ");
        return new RockPaperScissorGame(new Player1Move(splitted[0]), new Player2Move(splitted[1]));
    }

    static RockPaperScissorGame transformTask2(String line) {
        String[] splitted = line.split(" ");
        Player1Move player1Move = new Player1Move(splitted[0]);
        Player2Move player2Move = new Player2Move(switch (splitted[1]) {
            case "X" -> player1Move.rockPaperScissor().losingMoveAgainst();
            case "Y" -> player1Move.rockPaperScissor();
            case "Z" -> player1Move.rockPaperScissor().winningMoveAgainst();
            default -> throw new IllegalStateException();
        });

        return new RockPaperScissorGame(player1Move, player2Move);
    }

    int score() {
        return yourMove.rockPaperScissor().play(opponentMove.rockPaperScissor());
    }
}
