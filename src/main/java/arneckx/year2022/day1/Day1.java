package arneckx.year2022.day1;

import arneckx.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day1 extends Task<Integer, String> {

    private final List<Elv> elves = createElves();

    public Day1(String inputFile) {
        super(inputFile);
    }

    @Override
    protected Function<String, String> parser() {
        return line -> line;
    }

    @Override
    public Integer task1() {
        return this.elves.stream().mapToInt(Elv::totalCalories).max().orElseThrow();
    }

    @Override
    public Integer task2() {
        int[] ints = this.elves.stream().mapToInt(Elv::totalCalories).sorted().toArray();
        return ints[ints.length - 1] + ints[ints.length - 2] + ints[ints.length - 3];
    }

    private List<Elv> createElves() {
        List<Elv> elves = new ArrayList<>();
        Elv elv = new Elv();
        for (String line : input()) {
            if (line.isBlank()) {
                elves.add(elv);
                elv = new Elv();
            } else {
                elv.addFood(Integer.parseInt(line));
            }
        }
        return elves;
    }
}
