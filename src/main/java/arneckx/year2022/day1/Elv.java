package arneckx.year2022.day1;

import java.util.ArrayList;
import java.util.List;

public class Elv {

    private final List<Integer> calories = new ArrayList<>();
    private int totalCalories = 0;

    public void addFood(int calories){
        this.calories.add(calories);
        totalCalories += calories;
    }

    public int totalCalories() {
        return totalCalories;
    }
}
