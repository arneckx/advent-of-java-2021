package arneckx;

import java.util.List;
import java.util.function.Function;

public abstract class Task<R, M> {

    private final InputReader<M> reader = new InputReader<>(parser());
    private final String inputFile;

    protected Task(String inputFile) {
        this.inputFile = inputFile;
    }

    public abstract R task1();

    public abstract R task2();

    protected abstract Function<String, M> parser();

    protected Function<String, M> parserTask2(){
        return parser();
    }

    protected List<M> input() {
        return reader.read(inputFile);
    }

    protected List<M> inputTask2() {
        return new InputReader<>(parserTask2()).read(inputFile);
    }
}
