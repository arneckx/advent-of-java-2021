package arneckx.year2022.day1;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day1Test {

    private final String exampleFile = "/year2022/day1/example.txt";
    private final String inputFile = "/year2022/day1/input1.txt";
    private final Day1 input = new Day1(inputFile);
    private final Day1 example = new Day1(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(24000);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(70369);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(41000L);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(203002L);
    }
}
