package arneckx.year2022.day5;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day5Test {

    // TODO: [main] parse day automatically
    private final String exampleFile = "/year2022/day5/example.txt";
    private final String inputFile = "/year2022/day5/input1.txt";
    private final Day5 input = new Day5(inputFile);
    private final Day5 example = new Day5(exampleFile);

    @Test
    void task1Example() {
        String result = example.task1();

        assertThat(result).isEqualTo("CMZ");
    }

    @Test
    void task1() {
        String result = input.task1();

        assertThat(result).isNotEqualTo("QWBJVMSTN");
        assertThat(result).isEqualTo(8109);
    }

    @Test
    void task2Example() {
        String result = example.task2();

        assertThat(result).isEqualTo(70);
    }

    @Test
    void task2() {
        String result = input.task2();

        assertThat(result).isEqualTo(2738);
    }
}
