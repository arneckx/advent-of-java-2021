package arneckx.year2022.day3;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day3Test {

    private final String exampleFile = "/year2022/day3/example.txt";
    private final String inputFile = "/year2022/day3/input1.txt";
    private final Day3 input = new Day3(inputFile);
    private final Day3 example = new Day3(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(157);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(8109);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(70);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(2738);
    }
}
