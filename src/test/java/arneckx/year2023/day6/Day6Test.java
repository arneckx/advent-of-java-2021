package arneckx.year2023.day6;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day6Test {

    private final String exampleFile = "/year2023/day6/example.txt";
    private final String inputFile = "/year2023/day6/input.txt";
    private final Day6 input = new Day6(inputFile);
    private final Day6 example = new Day6(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(288);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(6209190);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(71503);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(28545089L);
    }
}
