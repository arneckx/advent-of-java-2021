package arneckx.year2023.day11;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day11Test {

    private final String exampleFile = "/year2023/day11/example.txt";
    private final String inputFile = "/year2023/day11/input.txt";
    private final Day11 input = new Day11(inputFile);
    private final Day11 example = new Day11(exampleFile);

    @Test
    void task1Example() {
        long result = example.task1();

        assertThat(result).isEqualTo(374L);
    }

    @Test
    void task1() {
        long result = input.task1();

        assertThat(result)
                .isEqualTo(9647174L);
    }

    @Test
    void task2Example() {
        long result = example.task2();

        assertThat(result).isEqualTo(82000210L);
    }

    @Test
    void task2() {
        long result = input.task2();

        assertThat(result).isEqualTo(377318892554L);
    }
}
