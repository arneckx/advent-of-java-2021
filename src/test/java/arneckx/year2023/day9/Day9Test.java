package arneckx.year2023.day9;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day9Test {

    private final String exampleFile = "/year2023/day9/example.txt";
    private final String inputFile = "/year2023/day9/input.txt";
    private final Day9 input = new Day9(inputFile);
    private final Day9 example = new Day9(exampleFile);

    @Test
    void task1Example() {
        long result = example.task1();

        assertThat(result).isEqualTo(114L);
    }

    @Test
    void task1() {
//        2174807970L
//        2174807968L
        long result = input.task1();
        assertThat(result).isEqualTo(2174807968L);
    }

    @Test
    void task2Example() {
        long result = example.task2();

        assertThat(result).isEqualTo(2L);
    }

    @Test
    void task2() {
        long result = input.task2();
//        1208L
//        1198L
        assertThat(result).isEqualTo(1208L);
    }
}
