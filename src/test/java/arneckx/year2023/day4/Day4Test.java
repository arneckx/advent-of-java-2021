package arneckx.year2023.day4;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day4Test {

    private final String exampleFile = "/year2023/day4/example.txt";
    private final String inputFile = "/year2023/day4/input.txt";
    private final Day4 input = new Day4(inputFile);
    private final Day4 example = new Day4(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(13);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(24160);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(30);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(5659035);
    }
}
