package arneckx.year2023.day2;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day2Test {

    private final String exampleFile = "/year2023/day2/example.txt";
    private final String inputFile = "/year2023/day2/input.txt";
    private final Day2 input = new Day2(inputFile);
    private final Day2 example = new Day2(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(8);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(2683);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(2286);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(49710);
    }
}
