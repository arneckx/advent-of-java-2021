package arneckx.year2023.day3;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day3Test {

    private final String exampleFile = "/year2023/day3/example.txt";
    private final String inputFile = "/year2023/day3/input.txt";
    private final Day3 input = new Day3(inputFile);
    private final Day3 example = new Day3(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(4361);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result)
                .isGreaterThan(268021)
                .isNotEqualTo(514324)
                .isNotEqualTo(515332)
                .isEqualTo(519444);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(467835);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(74528807);
    }

    @Test
    void NumberToCheckHasCorrectCoordinatesToCheck() {
        NumberToCheck numberToCheck1 = new NumberToCheck("617", 4, 0);
        NumberToCheck numberToCheck2 = new NumberToCheck("5", 4, 10);

        assertThat(numberToCheck1.toCheck())
                .containsExactlyInAnyOrder(
                        new Day3.Coordinate(3, -1),
                        new Day3.Coordinate(3, 0),
                        new Day3.Coordinate(3, 1),
                        new Day3.Coordinate(3, 2),
                        new Day3.Coordinate(3, 3),

                        new Day3.Coordinate(5, -1),
                        new Day3.Coordinate(5, 0),
                        new Day3.Coordinate(5, 1),
                        new Day3.Coordinate(5, 2),
                        new Day3.Coordinate(5, 3),

                        new Day3.Coordinate(4, -1),
                        new Day3.Coordinate(4, 3)
                );
        assertThat(numberToCheck2.toCheck())
                .containsExactlyInAnyOrder(
                        new Day3.Coordinate(3, 9),
                        new Day3.Coordinate(3, 10),
                        new Day3.Coordinate(3, 11),

                        new Day3.Coordinate(5, 9),
                        new Day3.Coordinate(5, 10),
                        new Day3.Coordinate(5, 11),

                        new Day3.Coordinate(4, 9),
                        new Day3.Coordinate(4, 11)
                );
    }
}
