package arneckx.year2023.day7;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day7Test {

    private final String exampleFile = "/year2023/day7/example.txt";
    private final String inputFile = "/year2023/day7/input.txt";
    private final Day7 input = new Day7(inputFile);
    private final Day7 example = new Day7(exampleFile);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(6440);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result)
                .isEqualTo(248569531);
    }

    @Test
    void task2Example() {
        int result = example.task2();

        assertThat(result).isEqualTo(5905);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result).isEqualTo(250382098);
    }
}
