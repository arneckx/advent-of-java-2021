package arneckx.year2023.day5;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day5Test {

    private final String exampleFile = "/year2023/day5/example.txt";
    private final String inputFile = "/year2023/day5/input.txt";
    private final Day5 input = new Day5(inputFile);
    private final Day5 example = new Day5(exampleFile);

    @Test
    void task1Example() {
        long result = example.task1();

        assertThat(result).isEqualTo(35L);
    }

    @Test
    void task1() {
        long result = input.task1();

        assertThat(result).isEqualTo(218513636L);
    }

    @Test
    void task2Example() {
        long result = example.task2();

        assertThat(result).isEqualTo(46);
    }

    @Test
    void task2() {
        long result = input.task2();

        assertThat(result).isEqualTo(81956384L);
    }
}
