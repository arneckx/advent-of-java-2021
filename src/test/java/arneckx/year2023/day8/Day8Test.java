package arneckx.year2023.day8;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day8Test {

    private final String exampleFile = "/year2023/day8/example.txt";
    private final String example2File = "/year2023/day8/example2.txt";
    private final String example3File = "/year2023/day8/example3.txt";
    private final String inputFile = "/year2023/day8/input.txt";
    private final Day8 input = new Day8(inputFile);
    private final Day8 example = new Day8(exampleFile);
    private final Day8 example2 = new Day8(example2File);
    private final Day8 example3 = new Day8(example3File);

    @Test
    void task1Example() {
        long result = example.task1();

        assertThat(result).isEqualTo(2L);
    }

    @Test
    void task1Example2() {
        long result = example2.task1();

        assertThat(result).isEqualTo(6L);
    }

    @Test
    void task1() {
        long result = input.task1();

        assertThat(result)
                .isEqualTo(12643L);
    }

    @Test
    void task2Example() {
        long result = example3.task2();

        assertThat(result).isEqualTo(6L);
    }

    @Test
    void task2() {
        long result = input.task2();

        assertThat(result).isEqualTo(13133452426987L);
    }
}
