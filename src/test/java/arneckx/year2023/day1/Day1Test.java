package arneckx.year2023.day1;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day1Test {

    private final String exampleFile = "/year2023/day1/example.txt";
    private final String example2File = "/year2023/day1/example2.txt";
    private final String inputFile = "/year2023/day1/input1.txt";
    private final arneckx.year2023.day1.Day1 input = new arneckx.year2023.day1.Day1(inputFile);
    private final arneckx.year2023.day1.Day1 example = new Day1(exampleFile);
    private final arneckx.year2023.day1.Day1 example2 = new Day1(example2File);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(142);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result).isEqualTo(53194);
    }

    @Test
    void task2Example() {
        int result = example2.task2();

        assertThat(result).isEqualTo(281);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result)
                .isNotEqualTo(54225)
                .isEqualTo(54249);
    }
}
