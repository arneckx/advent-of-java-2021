package arneckx.year2023.day10;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class Day10Test {

    private final String exampleFile = "/year2023/day10/example.txt";
    private final String example2File = "/year2023/day10/example2.txt";
    private final String example3File = "/year2023/day10/example3.txt";
    private final String example4File = "/year2023/day10/example4.txt";
    private final String example5File = "/year2023/day10/example5.txt";
    private final String inputFile = "/year2023/day10/input.txt";
    private final Day10 input = new Day10(inputFile);
    private final Day10 example = new Day10(exampleFile);
    private final Day10 example2 = new Day10(example2File);
    private final Day10 example3 = new Day10(example3File);
    private final Day10 example4 = new Day10(example4File);
    private final Day10 example5 = new Day10(example5File);

    @Test
    void task1Example() {
        int result = example.task1();

        assertThat(result).isEqualTo(4);
    }

    @Test
    void task1Example2() {
        int result = example2.task1();

        assertThat(result).isEqualTo(8);
    }

    @Test
    void task1() {
        int result = input.task1();

        assertThat(result)
                .isEqualTo(6806);
    }

    @Test
    void task2Example3() {
        int result = example3.task2();

        assertThat(result).isEqualTo(4);
    }

    @Test
    void task2Example4() {
        int result = example4.task2();

        assertThat(result).isEqualTo(8);
    }

    @Test
    void task2Example5() {
        int result = example5.task2();

        assertThat(result).isEqualTo(10);
    }

    @Test
    void task2() {
        int result = input.task2();

        assertThat(result)
                .isLessThan(475)
                .isEqualTo(250382098);
    }
}
