package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day8.Day8Task1;
import arneckx.year2021.day8.Day8Task2;
import arneckx.year2021.day8.FourDigitScreen;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class Day8Test {

    private final String exampleFile = "/year2021/day8/example.txt";
    private final String inputFile = "/year2021/day8/input1.txt";

    @Test
    void shouldDecodeCorrectly() {
        FourDigitScreen fourDigitScreen = new FourDigitScreen(
                Arrays.asList("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab".split(" ")),
                Arrays.asList("cdfeb fcadb cdfeb cdbaf".split(" "))
        );

        assertThat(fourDigitScreen.zero()).isEqualTo("cagedb");
        assertThat(fourDigitScreen.one()).isEqualTo("ab");
        assertThat(fourDigitScreen.three()).isEqualTo("fbcad");
        assertThat(fourDigitScreen.four()).isEqualTo("eafb");
        assertThat(fourDigitScreen.six()).isEqualTo("cdfgeb");
        assertThat(fourDigitScreen.seven()).isEqualTo("dab");
        assertThat(fourDigitScreen.eight()).isEqualTo("acedgfb");
        assertThat(fourDigitScreen.nine()).isEqualTo("cefabd");
        assertThat(fourDigitScreen.two()).isEqualTo("gcdfa");
        assertThat(fourDigitScreen.five()).isEqualTo("cdfbe");

        assertThat(fourDigitScreen.decode()).isEqualTo(5353);
    }

    @Test
    void fix() {
        FourDigitScreen fourDigitScreen = new FourDigitScreen(
                Arrays.asList("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd".split(" ")),
                Arrays.asList("ed bcgafe cdgba cbgef".split(" "))
        );

        assertThat(fourDigitScreen.decode()).isEqualTo(1625);
    }

    @Test
    void task1Example() {
        int result = new Day8Task1(exampleFile).execute();

        assertThat(result).isEqualTo(26);
    }

    @Test
    void task1() {
        int result = new Day8Task1(inputFile).execute();

        assertThat(result).isEqualTo(369);
    }

    @Test
    void task2Example() {
        int result = new Day8Task2(exampleFile).execute();

        assertThat(result).isEqualTo(61229);
    }

    @Test
    void task2() {
        int result = new Day8Task2(inputFile).execute();

        assertThat(result).isEqualTo(1031553);
    }
}
