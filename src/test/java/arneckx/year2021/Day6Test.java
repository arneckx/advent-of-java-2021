package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day6.Day6Task1;
import arneckx.year2021.day6.Day6Task2;
import org.junit.jupiter.api.Test;

class Day6Test {

    private final String exampleFile = "/year2021/day6/example.txt";
    private final String inputFile = "/year2021/day6/input1.txt";

    @Test
    void task1Example() {
        Long result = new Day6Task1(exampleFile).execute();

        assertThat(result).isEqualTo(5934L);
    }

    @Test
    void task1() {
        Long result = new Day6Task1(inputFile).execute();

        assertThat(result).isEqualTo(363101L);
    }

    @Test
    void task2Example() {
        long result = new Day6Task2(exampleFile).execute();

        assertThat(result).isEqualTo(26984457539L);
    }

    @Test
    void task2() {
        long result = new Day6Task2(inputFile).execute();

        assertThat(result).isEqualTo(1644286074024L);
    }
}
