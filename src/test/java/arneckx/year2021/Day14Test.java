package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day14.Day14Task1;
import arneckx.year2021.day14.Day14Task2;
import org.junit.jupiter.api.Test;

class Day14Test {

    private final String exampleFile = "/year2021/day14/example.txt";
    private final String inputFile = "/year2021/day14/input1.txt";

    @Test
    void task1Example() {
        long result = new Day14Task1(exampleFile).execute();

        assertThat(result).isEqualTo(1588);
    }

    @Test
    void task1() {
        long result = new Day14Task1(inputFile).execute();

        assertThat(result).isEqualTo(2745);
    }

    @Test
    void task2Example() {
        long result = new Day14Task2(exampleFile).execute();

        assertThat(result).isEqualTo(2188189693529L);
    }

    @Test
    void task2() {
        long result = new Day14Task2(inputFile).execute();

        assertThat(result).isEqualTo(3420801168962L);
    }
}
