package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day9.Day9Task1;
import arneckx.year2021.day9.Day9Task2;
import org.junit.jupiter.api.Test;

class Day9Test {

    private final String exampleFile = "/year2021/day9/example.txt";
    private final String inputFile = "/year2021/day9/input1.txt";

    @Test
    void task1Example() {
        int result = new Day9Task1(exampleFile).execute();

        assertThat(result).isEqualTo(15);
    }

    @Test
    void task1() {
        int result = new Day9Task1(inputFile).execute();

        assertThat(result).isEqualTo(478);
    }

    @Test
    void task2Example() {
        int result = new Day9Task2(exampleFile).execute();

        assertThat(result).isEqualTo(1134);
    }

    @Test
    void task2() {
        int result = new Day9Task2(inputFile).execute();

        assertThat(result)
                .isEqualTo(1327014);
    }
}
