package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day13.Day13Task1;
import arneckx.year2021.day13.Day13Task2;
import org.junit.jupiter.api.Test;

class Day13Test {

    private final String exampleFile = "/year2021/day13/example.txt";
    private final String inputFile = "/year2021/day13/input1.txt";

    @Test
    void task1Example() {
        int result = new Day13Task1(exampleFile).execute();

        assertThat(result).isEqualTo(17);
    }

    @Test
    void task1() {
        int result = new Day13Task1(inputFile).execute();

        assertThat(result).isEqualTo(847);
    }

    @Test
    void task2Example() {
        int result = new Day13Task2(exampleFile).execute();

        assertThat(result).isEqualTo(16);
    }

    @Test
    void task2() {
        int result = new Day13Task2(inputFile).execute();

        assertThat(result).isEqualTo(104);
    }
}
