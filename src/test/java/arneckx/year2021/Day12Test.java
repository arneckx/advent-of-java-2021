package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day12.Day12Task1;
import arneckx.year2021.day12.Day12Task2;
import org.junit.jupiter.api.Test;

class Day12Test {

    private final String exampleFile = "/year2021/day12/example.txt";
    private final String slightlyLargerExampleFile = "/year2021/day12/slightly-larger-example.txt";
    private final String largeExampleFile = "/year2021/day12/large-example.txt";
    private final String inputFile = "/year2021/day12/input1.txt";

    @Test
    void task1Example() {
        int result = new Day12Task1(exampleFile).execute();

        assertThat(result).isEqualTo(10);
    }

    @Test
    void task1SlightlyLargerExampleFile() {
        int result = new Day12Task1(slightlyLargerExampleFile).execute();

        assertThat(result).isEqualTo(19);
    }

    @Test
    void task1LargeExampleFile() {
        int result = new Day12Task1(largeExampleFile).execute();

        assertThat(result).isEqualTo(226);
    }

    @Test
    void task1() {
        int result = new Day12Task1(inputFile).execute();

        assertThat(result)
                .isGreaterThan(440)
                .isEqualTo(3298);
    }

    @Test
    void task2Example() {
        int result = new Day12Task2(exampleFile).execute();

        assertThat(result).isEqualTo(36);
    }

    @Test
    void task2SlightlyLargerExampleFile() {
        int result = new Day12Task2(slightlyLargerExampleFile).execute();

        assertThat(result).isEqualTo(103);
    }

    @Test
    void task2LargeExampleFile() {
        int result = new Day12Task2(largeExampleFile).execute();

        assertThat(result).isEqualTo(3509);
    }

    @Test
    void task2() {
        int result = new Day12Task2(inputFile).execute();

        assertThat(result)
                .isGreaterThan(5567)
                .isEqualTo(93572);
    }
}
