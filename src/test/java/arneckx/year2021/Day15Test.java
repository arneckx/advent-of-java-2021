package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day15.Day15Task1;
import arneckx.year2021.day15.Day15Task2;
import org.junit.jupiter.api.Test;

class Day15Test {

    private final String exampleFile = "/year2021/day15/example.txt";
    private final String inputFile = "/year2021/day15/input1.txt";

    @Test
    void task1Example() {
        int result = new Day15Task1(exampleFile).execute();

        assertThat(result).isEqualTo(40);
    }

    @Test
    void task1() {
        int result = new Day15Task1(inputFile).execute();

        assertThat(result)
                .isLessThan(577)
                .isLessThan(475)
                .isEqualTo(472);
    }

    @Test
    void task2Example() {
        int result = new Day15Task2(exampleFile).execute();

        assertThat(result).isEqualTo(315);
    }

    @Test
    void task2() {
        int result = new Day15Task2(inputFile).execute();

        assertThat(result).isEqualTo(2851);
    }
}
