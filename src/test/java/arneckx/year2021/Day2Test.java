package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day2.Day2Task1;
import arneckx.year2021.day2.Day2Task2;
import org.junit.jupiter.api.Test;

class Day2Test {

    private final String exampleFile = "/year2021/day2/example.txt";
    private final String inputFile = "/year2021/day2/input1.txt";

    @Test
    void task1Example() {
        int result = new Day2Task1(exampleFile).execute();

        assertThat(result).isEqualTo(150);
    }

    @Test
    void task1() {
        int result = new Day2Task1(inputFile).execute();

        assertThat(result).isEqualTo(1484118);
    }

    @Test
    void task2Example() {
        int result = new Day2Task2(exampleFile).execute();

        assertThat(result).isEqualTo(900);
    }

    @Test
    void task2() {
        int result = new Day2Task2(inputFile).execute();

        assertThat(result).isEqualTo(1463827010);
    }
}
