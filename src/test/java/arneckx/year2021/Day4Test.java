package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day4.BingoGame;
import arneckx.year2021.day4.Board;
import arneckx.year2021.day4.Day4Task1;
import arneckx.year2021.day4.Day4Task2;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class Day4Test {

    private final String exampleFile = "/year2021/day4/example.txt";
    private final String inputFile = "/year2021/day4/input1.txt";

    @Test
    void boardShouldGiveBingo() {
        BingoGame bingoGame = new BingoGame(new ArrayList<>(List.of(
                "17,25,31,22,79,72,58,47,62,50,30,91,11,63,66,83,33,75,44,18,56,81,32,46,93,13,41,65,14,95,19,38,8,35,52,7,12,70,84,23,4,42,90,60,6,40,97,16,27,86,5,48,54,64,29,67,26,89,99,53,34,0,57,3,92,37,59,9,21,78",
                "",
                "46 53 14 17 75 ",
                "71  4 70 99 48",
                "65 96  68  80  72",
                "3 97 62 37 88",
                "82  35 36  23 39  "
        )));
        Board winningBoard = bingoGame.play();

        assertThat(bingoGame.currentNumber()).isEqualTo(53);
        assertThat(winningBoard.sumOfAllUnmarkedNumbers()).isEqualTo(600);
    }

    @Test
    void columnHasBingo() {
        Board board = new Board();

        board.addRow("22 13 17 11  0 ");
        board.addRow("8  2 23  4 24 ");
        board.addRow("21  9 14 16  7 ");
        board.addRow("6 10  3 18  5 ");
        board.addRow("1 12 20 15 19  ");

        boolean number1 = board.markNumber(17);
        boolean number2 = board.markNumber(23);
        boolean number3 = board.markNumber(14);
        boolean number4 = board.markNumber(3);
        boolean number5 = board.markNumber(20);

        assertThat(number1).isFalse();
        assertThat(number2).isFalse();
        assertThat(number3).isFalse();
        assertThat(number4).isFalse();
        assertThat(number5).isTrue();
    }

    @Test
    void rowHasBingo() {
        Board board = new Board();

        board.addRow("22 13 17 11  0 ");
        board.addRow("8  2 23  4 24 ");
        board.addRow("21  9 14 16  7 ");
        board.addRow("6 10  3 18  5 ");
        board.addRow("1 12 20 15 19  ");

        boolean number1 = board.markNumber(8);
        boolean number2 = board.markNumber(23);
        boolean number3 = board.markNumber(2);
        boolean number4 = board.markNumber(4);
        boolean number5 = board.markNumber(24);

        assertThat(number1).isFalse();
        assertThat(number2).isFalse();
        assertThat(number3).isFalse();
        assertThat(number4).isFalse();
        assertThat(number5).isTrue();
    }

    @Test
    void bingoGameShouldHaveBingo() {
        BingoGame bingoGame = new BingoGame(new ArrayList<>(List.of(
                "13,2,9,10,12,7,10,34",
                "",
                "22 13 17 11  0 ",
                "8  2 23  4 24 ",
                "21  9 14 16  7 ",
                "6 10  3 18  5 ",
                "1 12 20 15 19  "
        )));
        Board winningBoard = bingoGame.play();

        assertThat(bingoGame.currentNumber()).isEqualTo(12);
        assertThat(winningBoard.sumOfAllUnmarkedNumbers()).isEqualTo(254);
    }

    @Test
    void task1Example() {
        int result = new Day4Task1(exampleFile).execute();

        assertThat(result).isEqualTo(4512);
    }

    @Test
    void task1() {
        int result = new Day4Task1(inputFile).execute();

        assertThat(result).isEqualTo(8442);
    }

    @Test
    void task2Example() {
        int result = new Day4Task2(exampleFile).execute();

        assertThat(result).isEqualTo(1924);
    }

    @Test
    void task2() {
        int result = new Day4Task2(inputFile).execute();

        assertThat(result).isEqualTo(4590);
    }
}
