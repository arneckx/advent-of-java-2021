package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day1.Day1Task1;
import arneckx.year2021.day1.Day1Task2;
import org.junit.jupiter.api.Test;

class Day1Test {

    private final String exampleFile = "/year2021/day1/example.txt";
    private final String inputFile = "/year2021/day1/input1.txt";

    @Test
    void task1ExampleShouldBe5() {
        int result = new Day1Task1(exampleFile).execute();

        assertThat(result).isEqualTo(7);
    }

    @Test
    void task1ShouldBe1752() {
        int result = new Day1Task1(inputFile).execute();

        assertThat(result).isEqualTo(1752);
    }

    @Test
    void task2ExampleShouldBe5() {
        int result = new Day1Task2(exampleFile).execute();

        assertThat(result).isEqualTo(5);
    }

    @Test
    void task2ShouldBe1781() {
        int result = new Day1Task2(inputFile).execute();

        assertThat(result).isEqualTo(1781);
    }
}
