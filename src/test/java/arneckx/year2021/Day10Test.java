package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day10.Day10Task1;
import arneckx.year2021.day10.Day10Task2;
import org.junit.jupiter.api.Test;

class Day10Test {

    private final String exampleFile = "/year2021/day10/example.txt";
    private final String inputFile = "/year2021/day10/input1.txt";

    @Test
    void whatHappens() {
        int result = new Day10Task1(exampleFile)
                .createChunks("{{([[([[[<(({<{}<>>[<>[]]}{([]<>)<<>{}>})<{{[]<>}{{}<>}}([()()]<<>[]>>>){{[[()<>]]<((){})<[]{}>>}{<<[]<>><()");

        assertThat(result).isEqualTo(25137);
    }

    @Test
    void accoladeIsIsCorrupted() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}](}}<<{{");

        assertThat(result).isEqualTo(1197);
    }

    @Test
    void bracketIsIsCorrupted() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}](]]<<{{");

        assertThat(result).isEqualTo(57);
    }

    @Test
    void parenthesisIsCorrupted() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}]())<<{{");

        assertThat(result).isEqualTo(3);
    }

    @Test
    void beakIsCorrupted() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}](><<{{");

        assertThat(result).isEqualTo(25137);
    }

    @Test
    void firstCharacterOfCorruptedLineIsReturned() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}](><<{{");

        assertThat(result).isEqualTo(25137);
    }

    @Test
    void nonFinishedLineShouldNotBeCorrupted() {
        int result = new Day10Task1(exampleFile)
                .createChunks("<{([([[(<>()){}](<<{{");

        assertThat(result).isZero();
    }

    @Test
    void task1Example() {
        int result = new Day10Task1(exampleFile).execute();

        assertThat(result).isEqualTo(26397);
    }

    @Test
    void task1() {
        int result = new Day10Task1(inputFile).execute();

        assertThat(result)
                .isLessThan(296475)
                .isEqualTo(294195);
    }

    @Test
    void task2Example() {
        long result = new Day10Task2(exampleFile).execute();

        assertThat(result).isEqualTo(288957);
    }

    @Test
    void task2() {
        long result = new Day10Task2(inputFile).execute();

        assertThat(result).isEqualTo(3490802734L);
    }
}
