package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day3.Day3Task1;
import arneckx.year2021.day3.Day3Task2;
import org.junit.jupiter.api.Test;

class Day3Test {

    private final String exampleFile = "/year2021/day3/example.txt";
    private final String inputFile = "/year2021/day3/input1.txt";

    @Test
    void task1Example() {
        int result = new Day3Task1(exampleFile).execute();

        assertThat(result).isEqualTo(198);
    }

    @Test
    void task1() {
        int result = new Day3Task1(inputFile).execute();

        assertThat(result).isEqualTo(3885894);
    }

    @Test
    void task2Example() {
        int result = new Day3Task2(exampleFile).execute();

        assertThat(result).isEqualTo(230);
    }

    @Test
    void task2() {
        int result = new Day3Task2(inputFile).execute();

        assertThat(result).isEqualTo(4375225);
    }
}
