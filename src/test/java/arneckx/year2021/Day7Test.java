package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day7.Day7Task1;
import arneckx.year2021.day7.Day7Task2;
import org.junit.jupiter.api.Test;

class Day7Test {

    private final String exampleFile = "/year2021/day7/example.txt";
    private final String inputFile = "/year2021/day7/input1.txt";

    @Test
    void task1Example() {
        int result = new Day7Task1(exampleFile).execute();

        assertThat(result).isEqualTo(37);
    }

    @Test
    void task1() {
        int result = new Day7Task1(inputFile).execute();

        assertThat(result).isEqualTo(356922);
    }

    @Test
    void task2Example() {
        int result = new Day7Task2(exampleFile).execute();

        assertThat(result).isEqualTo(168);
    }

    @Test
    void task2() {
        int result = new Day7Task2(inputFile).execute();

        assertThat(result)
                .isEqualTo(100347031);
    }
}
