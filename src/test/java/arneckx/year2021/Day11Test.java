package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day11.Day11Task1;
import arneckx.year2021.day11.Day11Task2;
import org.junit.jupiter.api.Test;

class Day11Test {

    private final String exampleFile = "/year2021/day11/example.txt";
    private final String inputFile = "/year2021/day11/input1.txt";

    @Test
    void task1Example() {
        int result = new Day11Task1(exampleFile).execute();

        assertThat(result).isEqualTo(1656);
    }

    @Test
    void task1() {
        int result = new Day11Task1(inputFile).execute();

        assertThat(result).isEqualTo(1785);
    }

    @Test
    void task2Example() {
        int result = new Day11Task2(exampleFile).execute();

        assertThat(result).isEqualTo(195);
    }

    @Test
    void task2() {
        int result = new Day11Task2(inputFile).execute();

        assertThat(result).isEqualTo(354);
    }
}
