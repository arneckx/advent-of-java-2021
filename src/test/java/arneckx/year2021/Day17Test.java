package arneckx.year2021;

import arneckx.year2021.day17.Day17Task1;
import arneckx.year2021.day17.Day17Task2;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
class Day17Test {

    private final String exampleFile = "/year2021/day17/example.txt";
    private final String inputFile = "/year2021/day17/input1.txt";

    @Test
    void task1Example() {
        int result = new Day17Task1(exampleFile).execute();

        assertThat(result).isEqualTo(45);
    }

    @Test
    void task1() {
        int result = new Day17Task1(inputFile).execute();

        assertThat(result).isEqualTo(6786);
    }

    @Test
    void task2Example() {
        long result = new Day17Task2(exampleFile).execute();

        assertThat(result).isEqualTo(112);
    }

    @Test
    void task2() {
        long result = new Day17Task2(inputFile).execute();

        assertThat(result).isEqualTo(2313);
    }
}
