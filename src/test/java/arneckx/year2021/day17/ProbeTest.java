package arneckx.year2021.day17;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ProbeTest {
    private final TargetArea targetArea = TargetArea.map("target area: x=20..30, y=-10..-5");

    @Test
    void shouldCrossTargetArea1() {
        Probe probe = Probe.launch(7, 2, targetArea);

        assertThat(probe.position()).isEqualTo(new Coordinate(28, -7));
        assertThat(probe.maximumYPosition()).isEqualTo(3);
        assertThat(probe.hasCrossedTarget()).isTrue();
    }

    @Test
    void shouldCrossTargetArea2() {
        Probe probe = Probe.launch(6, 3, targetArea);

        assertThat(probe.position()).isEqualTo(new Coordinate(21, -9));
        assertThat(probe.hasCrossedTarget()).isTrue();
    }

    @Test
    void shouldCrossTargetArea3() {
        Probe probe = Probe.launch(9, 0, targetArea);

        assertThat(probe.position()).isEqualTo(new Coordinate(30, -6));
        assertThat(probe.hasCrossedTarget()).isTrue();
    }

    @Test
    void shouldCrossTargetArea4() {
        Probe probe = Probe.launch(6, 9, targetArea);

        assertThat(probe.maximumYPosition()).isEqualTo(45);
        assertThat(probe.hasCrossedTarget()).isTrue();
    }

    @Test
    void shouldMissTargetArea() {
        Probe probe = Probe.launch(17, -4, targetArea);

        assertThat(probe.position()).isEqualTo(new Coordinate(48, -15));
        assertThat(probe.hasCrossedTarget()).isFalse();
    }
}
