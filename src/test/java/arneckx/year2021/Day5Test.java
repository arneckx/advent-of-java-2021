package arneckx.year2021;

import static org.assertj.core.api.Assertions.assertThat;

import arneckx.year2021.day5.Coordinate;
import arneckx.year2021.day5.Day5Task1;
import arneckx.year2021.day5.Day5Task2;
import arneckx.year2021.day5.Vent;
import org.junit.jupiter.api.Test;

class Day5Test {

    private final String exampleFile = "/year2021/day5/example.txt";
    private final String inputFile = "/year2021/day5/input1.txt";

    @Test
    void task1Test() {
        Vent vent1 = new Vent(
                false,
                new Coordinate(1, 1),
                new Coordinate(1, 3)
        );
        Vent vent2 = new Vent(
                false,
                new Coordinate(9, 7),
                new Coordinate(7, 7)
        );
        Vent vent3 = new Vent(
                false,
                new Coordinate(7, 9),
                new Coordinate(7, 7)
        );
        Vent vent4 = new Vent(
                false,
                new Coordinate(5, 9),
                new Coordinate(7, 9)
        );

        assertThat(vent1.coveredPoints()).containsOnly(
                new Coordinate(1, 1),
                new Coordinate(1, 2),
                new Coordinate(1, 3)
        );
        assertThat(vent2.coveredPoints()).containsOnly(
                new Coordinate(9, 7),
                new Coordinate(8, 7),
                new Coordinate(7, 7)
        );
        assertThat(vent3.coveredPoints()).containsOnly(
                new Coordinate(7, 7),
                new Coordinate(7, 8),
                new Coordinate(7, 9)
        );
        assertThat(vent4.coveredPoints()).containsOnly(
                new Coordinate(5, 9),
                new Coordinate(6, 9),
                new Coordinate(7, 9)
        );
    }

    @Test
    void task1Example() {
        int result = new Day5Task1(exampleFile).execute();

        assertThat(result).isEqualTo(5);
    }

    @Test
    void task1() {
        int result = new Day5Task1(inputFile).execute();

        assertThat(result).isEqualTo(6113);
    }

    @Test
    void task2Test2() {
        Vent vent1 = new Vent(
                true,
                new Coordinate(8, 0),
                new Coordinate(0, 8)
        );

        assertThat(vent1.coveredPoints()).containsExactlyInAnyOrder(
                new Coordinate(8, 0),
                new Coordinate(7, 1),
                new Coordinate(6, 2),
                new Coordinate(5, 3),
                new Coordinate(4, 4),
                new Coordinate(3, 5),
                new Coordinate(2, 6),
                new Coordinate(1, 7),
                new Coordinate(0, 8)
        );
    }

    @Test
    void task2Test() {
        Vent vent1 = new Vent(
                true,
                new Coordinate(1, 1),
                new Coordinate(3, 3)
        );
        Vent vent2 = new Vent(
                true,
                new Coordinate(9, 7),
                new Coordinate(7, 9)
        );
        Vent vent3 = new Vent(
                true,
                new Coordinate(3, 3),
                new Coordinate(1, 1)
        );
        Vent vent4 = new Vent(
                true,
                new Coordinate(7, 9),
                new Coordinate(9, 7)
        );

        assertThat(vent1.coveredPoints()).containsExactlyInAnyOrder(
                new Coordinate(1, 1),
                new Coordinate(2, 2),
                new Coordinate(3, 3)
        );
        assertThat(vent2.coveredPoints()).containsExactlyInAnyOrder(
                new Coordinate(9, 7),
                new Coordinate(8, 8),
                new Coordinate(7, 9)
        );
        assertThat(vent3.coveredPoints()).containsExactlyInAnyOrder(
                new Coordinate(1, 1),
                new Coordinate(2, 2),
                new Coordinate(3, 3)
        );
        assertThat(vent4.coveredPoints()).containsExactlyInAnyOrder(
                new Coordinate(9, 7),
                new Coordinate(8, 8),
                new Coordinate(7, 9)
        );
    }

    @Test
    void task2Test1() {
        Vent vent3 = new Vent(
                true,
                new Coordinate(212, 680),
                new Coordinate(212, 136)
        );
        Vent vent4 = new Vent(
                true,
                new Coordinate(18, 942),
                new Coordinate(943, 17)
        );
        Vent vent5 = new Vent(
                true,
                new Coordinate(942, 18),
                new Coordinate(17, 943)
        );
        Vent vent6 = new Vent(
                true,
                new Coordinate(0, 338),
                new Coordinate(1, 340)
        );

        assertThat(vent3.coveredPoints().size()).isEqualTo(545);
        assertThat(vent4.coveredPoints().size()).isEqualTo(926);
        assertThat(vent5.coveredPoints().size()).isEqualTo(926);
        assertThat(vent6.coveredPoints().size()).isZero();
    }

    @Test
    void task2Example() {
        int result = new Day5Task2(exampleFile).execute();

        assertThat(result).isEqualTo(12);
    }

    @Test
    void task2() {
        int result = new Day5Task2(inputFile).execute();

        assertThat(result).isEqualTo(20373);
    }
}
